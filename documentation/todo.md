### Features to implement
- Create a data endpoint
- Implement validation logic for service layer
- Request validation
- Return correct http status codes based on errors (Error Handling)

### Updates to make

### Improvements to make
- Improve the speed of the mapper tests
- Implement integration tests from the controller layer down 
- Add builders for domain objects in the test classes

### In Progress
- Complete the ids
- Implement deletes (Employee and Role)
- primaryRole needs to be a list

