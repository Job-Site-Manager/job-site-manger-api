USE JobSiteManager;

INSERT
	company (company_id, name, address, phone_number)
VALUES
	(1, "John's Earthmoving", "1223 Clarke Road", "519-453-4454");

INSERT
	role (role_id, name, notes, company_id)
VALUES
	(1, "Bulldozer Operator", "Requires a bulldozer", 1),
    (2, "Backhoe Operator", "Requires a backhoe", 1),
    (3, "Rock Truck Operator", null, 1),
    (4, "Scraper Operator", "Driver should not have a sore back", 1),
    (5, "Packer Operator", "Requires a packer", 1);

INSERT
	employee(employee_id, name, address, phone_number, company_id)
VALUES
	(1, "Tom Houston", "12 Steep Road", "519-425-6434", 1),
    (2, "Bill Crosby", "23 Rocky Road", "519-734-9921", 1),
    (3, "Wayne Gretzky", "99 Greztky Lane", "519-528-1884", 1),
    (4, "Tim Jefferson", "12 Smoke Cresent", "519-625-6434", 1),
    (5, "Susan Moore", "1 First Road", "519-625-6774", 1),
    (6, "Tom Rosen", "12 Half Road", "519-929-6484",  1),
    (7, "Auston Matthews", "7843 Dundas Steet", "519-725-6494", 1),
    (8, "Mitch Marner", "12 Steep Road", "519-625-6434", 1);


INSERT
	primary_role (employee_id, role_id)
VALUES
	(1, 1),
    (2, 2),
    (3, 4),
    (4, 3),
    (5, 4),
    (6, 1),
    (7, 5),
    (8, 5);


INSERT
	secondary_role (employee_id, role_id)
VALUES
	(1,  2),
    (1, 4),
    (2, 4),
    (2, 3),
    (2, 5),
    (4,  2),
    (5, 3),
    (5,  2),
    (6, 5),
    (6, 4),
    (7, 2),
    (8, 2);


INSERT
	job(job_id, name, address, company_id)
VALUES
	(1, "The Old Pond", "23 Pond Rd.", 1),
    (2, "Gravel Pit", "434 Beach Rd.", 1);


INSERT
	job_position(job_position_id, role_id, ideal_number_of_employees, job_id)
VALUES
	(1, 1, 2, 1),
    (2,2,1,1),
    (3, 3, 2,1),
    (4,5,2,2),
    (5,4,1,2),
    (6,1,1,2);


INSERT
	employee_position(employee_id, job_position_id)
VALUES
	(1, 1),
	(2, 2), 
    (3, 5),
    (4, 3),
    (5, 3),
    (6, 6),
    (7, 4),
    (8, 4);
    
    