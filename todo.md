# To do list 

## Branch: /feature/initial-construct

#### COMPLETED (Sept 18): Part One  
- Add Jobs, Employees and Roles
- Retrieve all jobs, all employees or all roles
- Retrieve individual jobs employees and roles by name

#### COMPLETED Part Two (B) 
- Set up gitlab / gitlab pipeline
- Write Unit Tests
- Remove all solvable TODOs 
- *scope increase* Add validation (Scope increase, need to still complete unit tests by friday..)


## In Progress Branch: /feature/database-integration 
- An actual database - Probably in docker
- The idea of multiple companies 

## Branch: /feature/logging 
- Implement logging for the application
- Error logging/debugging logging
 
## Branch: /feature/http-error-responses 
- Error handling - returning the correct responses
- Correct Responses for all requests

## Branch: /feature/authentication 
- Authentication 

## Branch: /feature/integrate-with-google-maps-api 
- Integrate with google api for locations
- Return distances from jobs sites for each employee
- Determine how to integrate with the API efficiently

## Break from project to implement the UI 


## Version Four (TBD)
- Look into algorithm for filling in positions based on distance, sinority and
positions


#### Validation:
- Looking at the article:
    - http://www.springboottutorial.com/spring-boot-validation-for-rest-services
    - https://medium.com/@jovannypcg/understanding-springs-controlleradvice-cd96a364033f
    - Have project spring-boot-2-rest-service-validation set up  
    
## To look into:
- Should nulls be updated to use optionals? What are optionals?
- Domain object/mappers, look into using constructor initialization and removing getters
    https://github.com/mybatis/old-google-code-issues/issues/480 


## Needs to be addressed:
- Integration tests
- Database:
    - How to store phone numbers
    - How to store google locations / Addresses in general

