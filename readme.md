# Job Site Manager API

Job Site Manager is a web application that helps companies who operate at multiple different job sites manage and track 
which employees are assigned to each site. Each employee in the system is assigned a set of roles that he/she can perform. 
When adding a new job site to the system you specifies the roles required for that site, and the number of employees need for 
each role. The application then helps you to fill the position with available, qualified employees. You can also update 
existing job sites as the demand for workers on the sites increase or decrease. 

This repository contains the API for the Job Site Manager application. The Job Site Manager API is a [Spring Boot](https://spring.io/projects/spring-boot) web app. 

Additional repositories for this application include:
- [Job Site Manager UI](https://gitlab.com/Job-Site-Manager/job-site-manager-ui)
- [Job Site Manager Deployment](https://gitlab.com/Job-Site-Manager/job-site-manager-deployment)

## Table of Contents
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installing](#installing)
- [Running the Tests](#running-the-tests)
- [Folder Structure](#folder-structure)
- [Branching](#branching)
- [Built With](#built-with)
- [Deployment](#deployment)
- [Versioning](#versioning)
- [Authors](#authors)


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
The following will need to be installed on your local environment:
- [Maven](https://nodejs.org/en/) 
- [Docker](https://www.docker.com/) 
- [Docker-Compose](https://docs.docker.com/compose/) 
- [Postman](https://www.getpostman.com/)

### Installing
(Documentation to come)

## Running the tests
(Documentation to come)

## Folder Structure

The basic structure is:
```
├── database                            // contains the Dockerfile and sql scripts that are needed to set up 
│                                       // and run a docker container that runs the applications mysql database
│  
├── documentation                       // contains a postman collection that can be imported into postman and                  
│                                       // used to call the api
├── src
│   ├─ main                    
│   │    ├─ java 
│   │    │    └─ api
│   │    │       ├─ config
│   │    │       ├─ controller
│   │    │       ├─ domain
│   │    │       │    └─ json
│   │    │       │        └─ mixins     // the mixins determine how the domain objects are serialized and deserialized
│   │    │       ├─ mapper              // defines the mapper interfaces, see the resource mapper files for the implementation
│   │    │       ├─ repository
│   │    │       ├─ service
│   │    │       ├─ util
│   │    │       ├─ validation
│   │    │       └─ Application.java 
│   │    │
│   │    └─ resources
│   │         ├─ mapper                  // contains the mybatis mapper xml files, these contain the sql queries for the app 
│   │         ├─ application.properties
│   │         └─ mybatis-config.xml
│   └── test                
│        ├─ java 
│        │    ├─ api
│        │    │  ├─ config
│        │    │  ├─ controller
│        │    │  ├─ domain
│        │    │  │    ├─ data            // contains the java objects used for testing, java objects in this folder should  
│        │    │  │    │                  // always match the data defined the in the test/resource/sql-scripts
│        │    │  │    └─ json
│        │    │  │        └─ mixins
│        │    │  ├─ mapper
│        │    │  ├─ repository
│        │    │  ├─ service
│        │    │  ├─ util
│        │    │  └─ validation
│        │    │
│        │    └─ test_utilities  
│        │
│        └─ resources
│             ├─ json                    // contains json test data used by the mixin tests and the controller tests
│             └─ sql-scripts             // scripts used to populate the database with data for the mapper tests
│ 
└── pom.xml
```

## Branching 
This project uses the [gitflow branching](https://medium.com/@muneebsajjad/git-flow-explained-quick-and-simple-7a753313572f) pattern. 

When creating a personal branch off of a feature branch the following name pattern should be used:
*{developers-name}/{name-of-feature-branched-off-of}/{branch-description}*. For example if I branched off of the 
feature branch *feature/adding-jobs-page* to do work on the jobs form an appropriate name for the branch would be 
*eric/adding-jobs-page/creating-job-form*.

## Built With
- [Spring-Boot](https://spring.io/projects/spring-boot)
- [Maven](https://maven.apache.org/)
- [MyBatis](http://www.mybatis.org/mybatis-3/)
- [MySql](https://www.mysql.com/)
- [JUnit](https://junit.org/junit5/)
- [Mockito](https://site.mockito.org/)
- [AssertJ](http://joel-costigliola.github.io/assertj/)

## Deployment 
(Documentation to come)

## Versioning
(Documentation to come)

## Authors
- Eric Lammers (ericjameslammers@gmail.com)

