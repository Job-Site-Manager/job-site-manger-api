package api.config;

import api.domain.Employee;
import api.domain.JobPosition;
import api.domain.Job;
import api.domain.Role;
import api.domain.json.mixins.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
public class JacksonConfig {

    @Bean
    // TODO is this necessary? Why are there two, this was causing EmployeeControllerTest to fail
    @Primary
    public Jackson2ObjectMapperBuilder createJacksonBuilder() {
        Jackson2ObjectMapperBuilder mapperBuilder = new Jackson2ObjectMapperBuilder();

        mapperBuilder.indentOutput(true);

        mapperBuilder
            .mixIn(Role.class, RoleMixin.class)
            .mixIn(Employee.class, EmployeeMixin.class)
            .mixIn(JobPosition.class, JobPositionMixin.class)
            .mixIn(Job.class, JobMixin.class);

        return mapperBuilder;
    }

}
