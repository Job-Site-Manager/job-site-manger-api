package api.mapper;

import api.domain.Employee;
import api.domain.Job;
import api.domain.JobPosition;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Mapper
@Component
public interface JobMapper {

    List<Job> selectAllJobs(int companyId);

    Job selectJobById(@Param("jobId") int jobId, @Param("companyId") int companyId);

    Integer selectJobPositionIdByJobIdAndRoleId(@Param("jobId") int jobId, @Param("roleId") int roleId);

    JobPosition selectJobPositionById(@Param("jobPositionId") int jobPositionId, @Param("companyId") int companyId);

    void insertJobWithoutJobPositions(@Param("job") Job job, @Param("companyId") int companyId);

    void insertJobPositionsWithoutTheEmployeeLists(
        @Param("jobPositions") Set<JobPosition> jobPositions,
        @Param("jobId") int jobId,
        @Param("companyId") int companyId
    );

    void insertTheEmployeeListForAJobPosition(
        @Param("employeeSet") Set<Employee> employeeSet,
        @Param("jobPositionId") int jobPositionId,
        @Param("companyId") int companyId
    );

    boolean updateJobDetails(@Param("job") Job job, @Param("companyId") int companyId);

    String retrieveMatchingName(@Param("jobName") String jobName, @Param("companyId") int companyId);

    boolean deleteJob(@Param("jobId") int jobId, @Param("companyId") int companyId);

    boolean deleteJobPositions(@Param("jobId") int jobId);
}
