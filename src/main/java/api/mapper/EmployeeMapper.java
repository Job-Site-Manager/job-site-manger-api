package api.mapper;

import api.domain.Employee;
import api.domain.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Mapper
@Component
public interface EmployeeMapper {

    List<Employee> selectAllEmployees(int companyId);

    Employee selectEmployeeById(@Param("employeeId") int employeeId, @Param("companyId") int companyId);

    List<String> retrieveMatchingNames(@Param("nameList") List<String> nameList, @Param("companyId") int companyId);

    void insertEmployeeWithoutRoles(@Param("employee") Employee employee, @Param("companyId") int companyId);

    void insertPrimaryRole(@Param("employeeId") int employeeId, @Param("primaryRole") Role primaryRole, @Param("companyId") int companyId);

    void insertSecondaryRoles(@Param("employeeId") int employeeId, @Param("roleSet") Set<Role> roleSet, @Param("companyId") int companyId);

    boolean deleteEmployee(@Param("employeeId") int employeeId, @Param("companyId") int companyId);

    boolean deletePrimaryRole(@Param("employeeId") int employeeId);

    boolean deleteSecondaryRoles(@Param("employeeId") int employeeId);

    boolean updateEmployeeDetails(@Param("employee") Employee employee, @Param("companyId") int companyId);

}
