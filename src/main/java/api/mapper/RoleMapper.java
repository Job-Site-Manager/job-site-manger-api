package api.mapper;

import api.domain.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Mapper
@Component
public interface RoleMapper {

    List<Role> selectAllRoles(int companyId);

    Role selectRoleById(@Param("id") int id, @Param("companyId") int companyId);

    Integer selectRoleIdByName(@Param("name") String name, @Param("companyId") int companyId);

    boolean insertRole(@Param("role") Role role, @Param("companyId") int companyId);

    Set<String> retrieveMatchingNames(@Param("nameSet") Set<String> nameList, @Param("companyId") int companyId);

    boolean deleteRole(@Param("id") int id, @Param("companyId") int companyId);

    boolean updateRole(@Param("role") Role role, @Param("companyId") int companyId);
}
