package api.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.isNull;

public class ListUtil {

    private ListUtil() {

    }

    public static boolean areListsEqual(List<String> listOne, List<String> listTwo) {
        if (isNull(listOne) && isNull(listTwo)){
            return true;
        }
        else if(isNull(listOne) || isNull(listTwo) || (listOne.size() != listTwo.size())){
            return false;
        }

        List<String> listOneCopy = new ArrayList<>(listOne);
        List<String> listTwoCopy = new ArrayList<>(listTwo);

        Collections.sort(listOneCopy);
        Collections.sort(listTwoCopy);

        return listOneCopy.equals(listTwoCopy);
    }

}
