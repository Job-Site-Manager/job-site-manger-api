package api.util;

public class URL {

    private URL() {
    }

    public static String replacePlusSignsWithSpaces(String urlSubstring) throws Exception {
        return java.net.URLDecoder.decode(urlSubstring, "UTF-8");
    }

}
