package api.service;

import api.domain.Role;
import api.repository.AllRoles;
import api.validation.role.RoleValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoleServiceImpl implements RoleService {

    private AllRoles allRoles;
    private RoleValidation roleValidation;

    @Autowired
    public RoleServiceImpl(AllRoles allRoles, RoleValidation roleValidation) {
        this.allRoles = allRoles;
        this.roleValidation = roleValidation;
    }

    @Override
    public List<Role> getAllRoles() {
        return allRoles.getAll();
    }

    @Override
    public Role getRole(int id) {
        return allRoles.get(id);
    }

    @Override
    public Role addRole(Role role) {
        roleValidation.validateNewRole(role);

        return allRoles.add(role);
    }

    @Override
    public boolean deleteRole(int id) {
        return allRoles.delete(id);
    }

    @Override
    public boolean updateRole(Role role) {
        return allRoles.update(role);
    }

}
