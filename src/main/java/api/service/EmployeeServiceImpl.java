package api.service;

import api.domain.Employee;
import api.domain.Role;
import api.repository.AllEmployees;
import api.repository.AllRoles;
import api.validation.employee.EmployeeValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class EmployeeServiceImpl implements EmployeeService {

    private AllEmployees allEmployees;
    private EmployeeValidation employeeValidation;

    @Autowired
    public EmployeeServiceImpl(AllEmployees allEmployees, EmployeeValidation employeeValidation) {
        this.allEmployees = allEmployees;
        this.employeeValidation = employeeValidation;
    }

    @Override
    public List<Employee> getAllEmployees() {
        return allEmployees.getAll();
    }

    @Override
    public Employee getEmployee(int id) {
        return allEmployees.get(id);
    }

    @Override
    public Employee addEmployee(Employee employee) {
        employeeValidation.validateNewEmployee(employee);

        return allEmployees.add(employee);
    }

    @Override
    public boolean deleteEmployee(int id) {
        return allEmployees.delete(id);
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        return allEmployees.update(employee);
    }

}
