package api.service;

import api.domain.Job;
import api.validation.job.JobValidation;
import api.repository.AllJobs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JobServiceImpl implements  JobService {

    private AllJobs allJobs;
    private JobValidation jobValidation;

    @Autowired
    public JobServiceImpl(AllJobs allJobs, JobValidation jobValidation) {
        this.allJobs = allJobs;
        this.jobValidation = jobValidation;
    }

    @Override
    public List<Job> getAllJobs() {
        return allJobs.getAll();
    }

    @Override
    public Job getJob(int id) {
        return allJobs.get(id);
    }

    @Override
    public Job addJob(Job job) {
        jobValidation.validateNewJob(job);

        return allJobs.add(job);
    }

    @Override
    public Job updateJob(Job job) {
        return allJobs.update(job);
    }

    @Override
    public boolean deleteJob(int id) {
        return allJobs.delete(id);
    }
}
