package api.service;

import api.domain.Job;

import java.util.List;

public interface JobService {

    List<Job> getAllJobs();

    Job getJob(int id);

    Job addJob(Job job);

    Job updateJob(Job job);

    boolean deleteJob(int id);
}
