package api.service;

import api.domain.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> getAllEmployees();

    Employee getEmployee(int id);

    Employee addEmployee(Employee employee);

    boolean deleteEmployee(int id);

    Employee updateEmployee(Employee employee);

}
