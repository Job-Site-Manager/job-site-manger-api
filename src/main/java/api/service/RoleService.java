package api.service;

import api.domain.Role;

import java.util.List;

public interface RoleService {

    List<Role> getAllRoles();

    Role getRole(int id);

    Role addRole(Role role);

    boolean deleteRole(int id);

    boolean updateRole(Role role);

}
