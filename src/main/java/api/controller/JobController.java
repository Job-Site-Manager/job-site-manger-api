package api.controller;

import api.domain.Job;
import api.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class JobController {

    private JobService jobService;

    @Autowired
    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    @RequestMapping(value = "/jobs", method = GET)
    public List<Job> getAllJobs() {
        return jobService.getAllJobs();
    }

    @RequestMapping(value = "/job/{id}", method = GET)
    public Job getJobByName(@PathVariable("id") int id) throws Exception {
        return jobService.getJob(id);
    }

    @RequestMapping(value="/job", method = POST)
    public Job addJob(@Valid @RequestBody Job job) {
        return jobService.addJob(job);
    }

    @RequestMapping(value = "/job/{id}", method = DELETE)
    public boolean deleteJob(@PathVariable("id") int id) {
        return jobService.deleteJob(id);
    }

    @RequestMapping(value="/job", method = PUT)
    public Job updateJob(@RequestBody Job job) {
        return jobService.updateJob(job);
    }

}
