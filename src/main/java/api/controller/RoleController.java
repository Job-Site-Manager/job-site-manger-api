package api.controller;

import api.domain.Role;
import api.service.RoleService;
import api.util.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class RoleController {

    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @RequestMapping(value = "/roles", method = GET)
    public List<Role> getAllRoles() {
        return roleService.getAllRoles();
    }

    @RequestMapping(value = "/role/{id}", method = GET)
    public Role getRoleById(@PathVariable("id") int id) {
        return roleService.getRole(id);
    }

    @RequestMapping(value = "/role", method = POST)
    public Role addRole(@RequestBody Role role) {
        return roleService.addRole(role);
    }

    @RequestMapping(value = "/role/{id}", method = DELETE)
    public boolean deleteRole(@PathVariable("id") int id) {
        return roleService.deleteRole(id);
    }

    @RequestMapping(value = "/role", method = PUT)
    public boolean updateRole(@RequestBody Role role) {
        return roleService.updateRole(role);
    }

}
