package api.validation.role.helpers;

import api.domain.Role;
import api.repository.AllRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;

@Component
public class NewRoleValidation {

    private AllRoles allRoles;

    @Autowired
    public NewRoleValidation(AllRoles allRoles) {
        this.allRoles = allRoles;
    }

    public void validate(Role role) {
        checkThatTheRoleNameIsUnique(role.getName());
    }

    private void checkThatTheRoleNameIsUnique(String roleName) {
        if(allRoles.contains(new HashSet<>(Collections.singletonList(roleName)))){
            throw new IllegalStateException("Cannot add role. A role with that name already exists.");
        }
    }

}
