package api.validation.role;

import api.domain.Role;
import api.validation.role.helpers.NewRoleValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RoleValidation {

    private NewRoleValidation newRoleValidation;

    @Autowired
    public RoleValidation(NewRoleValidation newRoleValidation) {
        this.newRoleValidation = newRoleValidation;
    }

    public void validateNewRole(Role role) {
        newRoleValidation.validate(role);
    }

}
