package api.validation.job;

import api.domain.Job;
import api.validation.job.helpers.NewJobValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// TODO: Possible - Employees can't be added if they are already part of another job
@Component
public class JobValidation {

    private NewJobValidation newJobValidation;

    @Autowired
    public JobValidation(NewJobValidation newJobValidation) {
        this.newJobValidation = newJobValidation;
    }

    public void validateNewJob(Job job) {
        newJobValidation.validate(job);
    }

}
