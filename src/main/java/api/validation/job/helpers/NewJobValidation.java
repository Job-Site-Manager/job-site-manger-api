package api.validation.job.helpers;

import api.domain.Employee;
import api.domain.Job;
import api.domain.JobPosition;
import api.repository.AllEmployees;
import api.repository.AllJobs;
import api.repository.AllRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class NewJobValidation {

    private AllJobs allJobs;
    private AllEmployees allEmployees;
    private AllRoles allRoles;

    @Autowired
    public NewJobValidation(AllJobs allJobs, AllEmployees allEmployees, AllRoles allRoles) {
        this.allJobs = allJobs;
        this.allEmployees = allEmployees;
        this.allRoles = allRoles;
    }

    public void validate(Job job) {
        checkThatTheJobNameIsUnique(job.getName());
        validateSetOfJobPositions(job.getJobPositions());
    }

    private void validateSetOfJobPositions(Set<JobPosition> jobPositions) {
        checkThatTheEmployeesExist(jobPositions);
        checkThatTheRolesExist(jobPositions);
        checkForDuplicateJobPositions(jobPositions);
    }

    private void checkThatTheJobNameIsUnique(String jobName) {
        if(allJobs.contains(jobName)) {
            throw new IllegalStateException("Cannot add job. A job with that name already exists.");
        }
    }

    private void checkThatTheEmployeesExist(Set<JobPosition> jobPositions) {
        List<String> allEmployeesAssignedToTheJob = new ArrayList<>();

        jobPositions.forEach(jobPosition -> allEmployeesAssignedToTheJob.addAll(
            jobPosition
                .getEmployeeSet()
                .stream()
                .map(Employee::getName)
                .collect(Collectors.toList())
        ));

        if(!allEmployees.contains(allEmployeesAssignedToTheJob)){
            throw new IllegalStateException("Cannot add job. One of the employees specified does not exist.");
        }
    }

    private void checkThatTheRolesExist(Set<JobPosition> jobPositions) {
        Set<String> allRolesAssignedToTheJob = new HashSet<>();

        jobPositions.forEach(jobPosition -> allRolesAssignedToTheJob.add(
            jobPosition.getRole().getName()
        ));

        if(!allRoles.contains(allRolesAssignedToTheJob)){
            throw new IllegalStateException("Cannot add job. One of the roles specified does not exist.");
        }
    }

    private void checkForDuplicateJobPositions(Set<JobPosition> jobPositions) {
        Set<String> roleNames = new HashSet<>();

        jobPositions.forEach(jobPosition -> {
            String roleName = jobPosition.getRole().getName();
            if(!roleNames.add(roleName)) {
                throw new IllegalStateException(
                    createErrorMessageForMultipleJobPositionsWithTheSameRole(roleName)
                );
            }
        });
    }

    private String createErrorMessageForMultipleJobPositionsWithTheSameRole(String roleName) {
        return String.format(
            "Cannot add job. There are multiple job positions with the role: %s",
            roleName
        );
    }

}
