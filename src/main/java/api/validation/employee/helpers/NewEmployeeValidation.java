package api.validation.employee.helpers;

import api.domain.Employee;
import api.domain.Role;
import api.repository.AllRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class NewEmployeeValidation {

    private AllRoles allRoles;

    @Autowired
    public NewEmployeeValidation(AllRoles allRoles) {
        this.allRoles = allRoles;
    }

    public void validate(Employee employee) {
        // TODO: Complete Validation, need to check that no employee with this name already exists
        // Need to also check that there are not repeat primary and secondary roles
        checkThatAssignedRolesExist(employee);
    }

    private void checkThatAssignedRolesExist(Employee employee) {
        if(!allRoles.contains(new HashSet<>(Collections.singletonList(employee.getPrimaryRole().getName())))) {
            throw new IllegalStateException("Failed to add employee. The specified primary role does not exist.");
        }

        Set<String> secondaryRoleNames = mapSetOfRolesToSetOfRoleNames(employee.getSecondaryRoles());
        if(!allRoles.contains(secondaryRoleNames)) {
            throw new IllegalStateException("Failed to add employee. At least one of the specified secondary roles does not exist.");
        }
    }

    private Set<String> mapSetOfRolesToSetOfRoleNames(Set<Role> roleList) {
        return roleList.stream().map(Role::getName).collect(Collectors.toSet());
    }

}
