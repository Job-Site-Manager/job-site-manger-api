package api.validation.employee;

import api.domain.Employee;
import api.validation.employee.helpers.NewEmployeeValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeValidation {

    private NewEmployeeValidation newEmployeeValidation;

    @Autowired
    public EmployeeValidation(NewEmployeeValidation newEmployeeValidation) {
        this.newEmployeeValidation = newEmployeeValidation;
    }

    public void validateNewEmployee(Employee employee) {
        newEmployeeValidation.validate(employee);
    }

}
