package api.domain;

import java.util.Objects;
import java.util.Set;

public class JobPosition {

    private int id;
    private Role role;
    private int idealNumberOfEmployees;
    private Set<Employee> employeeSet;

    public JobPosition() {

    }

    public JobPosition(int id, Role role, int idealNumberOfEmployees, Set<Employee> employeeSet) {
        this.id = id;
        this.role = role;
        this.idealNumberOfEmployees = idealNumberOfEmployees;
        this.employeeSet = employeeSet;
    }

    public JobPosition(Role role, int idealNumberOfEmployees, Set<Employee> employeeSet) {
        this.role = role;
        this.idealNumberOfEmployees = idealNumberOfEmployees;
        this.employeeSet = employeeSet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getIdealNumberOfEmployees() {
        return idealNumberOfEmployees;
    }

    public void setIdealNumberOfEmployees(int idealNumberOfEmployees) {
        this.idealNumberOfEmployees = idealNumberOfEmployees;
    }

    public Set<Employee> getEmployeeSet() {
        return employeeSet;
    }

    public void setEmployeeSet(Set<Employee> employeeSet) {
        this.employeeSet = employeeSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobPosition that = (JobPosition) o;
        return id == that.id &&
            idealNumberOfEmployees == that.idealNumberOfEmployees &&
            Objects.equals(role, that.role) &&
            Objects.equals(employeeSet, that.employeeSet);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, role, idealNumberOfEmployees, employeeSet);
    }

}