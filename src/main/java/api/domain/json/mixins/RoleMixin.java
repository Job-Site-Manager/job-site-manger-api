package api.domain.json.mixins;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class RoleMixin {

    // TODO: Do we need multiple constructors?

    @JsonCreator
    public RoleMixin(
        @JsonProperty("name") String name,
        @JsonProperty("notes") String notes,
        @JsonProperty("id") int id
    ) {
    }

}
