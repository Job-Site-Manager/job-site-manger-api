package api.domain.json.mixins;

import api.domain.Employee;
import api.domain.Role;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public abstract class JobPositionMixin {

    @JsonCreator
    public JobPositionMixin(
        @JsonProperty("role") Role role,
        @JsonProperty("idealNumberOfEmployees") int idealNumberOfEmployees,
        @JsonProperty("employees") Set<Employee> employeeSet) {
    }

    @JsonProperty("employees")
    public abstract Set<Employee> getEmployeeSet();

}
