package api.domain.json.mixins;

import api.domain.JobPosition;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public abstract class JobMixin {

    @JsonCreator
    public JobMixin(
        @JsonProperty("name") String name,
        @JsonProperty("address") String address,
        @JsonProperty("jobPositions") Set<JobPosition> jobPositions) {
    }

}
