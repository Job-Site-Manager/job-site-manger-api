package api.domain.json.mixins;

import api.domain.Role;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public abstract class EmployeeMixin {

    @JsonCreator
    public EmployeeMixin(
        @JsonProperty("id") int id,
        @JsonProperty("name") String name,
        @JsonProperty("address") String address,
        @JsonProperty("phoneNumber") String phoneNumber,
        @JsonProperty("primaryRole") Role primaryRole,
        @JsonProperty("secondaryRoles") Set<Role> secondaryRoles) {
    }
}