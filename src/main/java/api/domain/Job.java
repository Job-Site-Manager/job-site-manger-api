package api.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

public class Job {

    private int id;
    @Size(min=2, message="Name should have at least 2 characters")
    @NotNull private String name;
    @NotNull private String address;
    @NotNull private Set<JobPosition> jobPositions;

    public Job(){
    }

    public Job(int id, String name, String address, Set<JobPosition> jobPositions) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.jobPositions = jobPositions;
    }

    public Job(String name, String address, Set<JobPosition> jobPositions) {
        this.name = name;
        this.address = address;
        this.jobPositions = jobPositions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<JobPosition> getJobPositions() {
        return jobPositions;
    }

    public void setJobPositions(Set<JobPosition> jobPositions) {
        this.jobPositions = jobPositions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job job = (Job) o;
        return id == job.id &&
            Objects.equals(name, job.name) &&
            Objects.equals(address, job.address) &&
            Objects.equals(jobPositions, job.jobPositions);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, address, jobPositions);
    }
}
