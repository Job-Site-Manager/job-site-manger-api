package api.domain;

import java.util.Objects;
import java.util.Set;

public class Employee {

    private int id;
    private String name;
    private String address;
    private String phoneNumber;
    private Role primaryRole;
    private Set<Role> secondaryRoles;

    public Employee() { }

    public Employee(
        int id,
        String name,
        String address,
        String phoneNumber,
        Role primaryRole,
        Set<Role> secondaryRoles
    ) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.primaryRole = primaryRole;
        this.secondaryRoles = secondaryRoles;
    }

    public Employee(
        String name,
        String address,
        String phoneNumber,
        Role primaryRole,
        Set<Role> secondaryRoles
    ) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.primaryRole = primaryRole;
        this.secondaryRoles = secondaryRoles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Role getPrimaryRole() {
        return primaryRole;
    }

    public void setPrimaryRole(Role primaryRole) {
        this.primaryRole = primaryRole;
    }

    public Set<Role> getSecondaryRoles() {
        return secondaryRoles;
    }

    public void setSecondaryRoles(Set<Role> secondaryRoles) {
        this.secondaryRoles = secondaryRoles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id &&
            Objects.equals(name, employee.name) &&
            Objects.equals(address, employee.address) &&
            Objects.equals(phoneNumber, employee.phoneNumber) &&
            Objects.equals(primaryRole, employee.primaryRole) &&
            Objects.equals(secondaryRoles, employee.secondaryRoles);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, address, phoneNumber, primaryRole, secondaryRoles);
    }
}
