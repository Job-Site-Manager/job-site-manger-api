package api.domain;

import java.util.Objects;

public class Role {

    private String name;
    private String notes;
    private int id;

    public Role(String name, int id) {
        this(name, null, id);
    }

    public Role(String name, String notes) {
        this.name = name;
        this.notes = notes;
    }

    public Role(String name, String notes, int id) {
        this.name = name;
        this.notes = notes;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return id == role.id &&
            Objects.equals(name, role.name) &&
            Objects.equals(notes, role.notes);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, notes, id);
    }
}
