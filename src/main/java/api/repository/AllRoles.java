package api.repository;

import api.domain.Role;

import java.util.List;
import java.util.Set;

public interface AllRoles {

    List<Role> getAll();

    Role get(int id);

    Role add(Role role);

    boolean contains(Set<String> roleNames);

    boolean delete(int id);

    boolean update(Role role);

}
