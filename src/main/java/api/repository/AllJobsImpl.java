package api.repository;

import api.config.Profiles;
import api.domain.Job;
import api.domain.JobPosition;
import api.mapper.JobMapper;
import api.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Objects.isNull;

@Component
@Profile(Profiles.DATABASE)
public class AllJobsImpl implements AllJobs {

    private int companyId = 1;

    private JobMapper jobMapper;
    private RoleMapper roleMapper;

    @Autowired
    public AllJobsImpl(JobMapper jobMapper, RoleMapper roleMapper) {
        this.jobMapper = jobMapper;
        this.roleMapper = roleMapper;
    }

    @Override
    public List<Job> getAll() {
        return jobMapper.selectAllJobs(companyId);
    }

    @Override
    public Job get(int id) {
        return jobMapper.selectJobById(id, companyId);
    }

    @Override
    public Job add(Job job) {
        jobMapper.insertJobWithoutJobPositions(job, companyId);

        addJobPositions(job.getId(), job);

        return jobMapper.selectJobById(job.getId(), companyId);
    }

    @Override
    public Job update(Job job) {
        jobMapper.updateJobDetails(job, companyId);

        jobMapper.deleteJobPositions(job.getId());
        addJobPositions(job.getId(), job);

        return jobMapper.selectJobById(job.getId(), companyId);
    }

    private void addJobPositions(int jobId, Job job) {
        jobMapper.insertJobPositionsWithoutTheEmployeeLists(job.getJobPositions(), jobId, companyId);

        job.getJobPositions().forEach(
            jobPosition -> addEmployeeListForJobPosition(jobPosition, jobId)
        );
    }

    private void addEmployeeListForJobPosition(JobPosition jobPosition, int jobId) {
        if(jobPosition.getEmployeeSet().isEmpty()) {
            return;
        }

        int jobPositionId = jobMapper.selectJobPositionIdByJobIdAndRoleId(
            jobId, roleMapper.selectRoleIdByName(jobPosition.getRole().getName(), companyId)
        );

        jobMapper.insertTheEmployeeListForAJobPosition(
            jobPosition.getEmployeeSet(),
            jobPositionId,
            companyId
        );
    }

    @Override
    public boolean delete(int id) {
        return jobMapper.deleteJob(id, companyId);
    }

    @Override
    public boolean contains(String jobName) {
        if(isNull(jobName)) {
            return false;
        }

        String existingJobName = jobMapper.retrieveMatchingName(jobName, companyId);

        return jobName.equals(existingJobName);
    }

}
