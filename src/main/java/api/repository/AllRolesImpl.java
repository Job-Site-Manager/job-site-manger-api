package api.repository;

import api.config.Profiles;
import api.domain.Role;
import api.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.*;

import static java.util.Objects.isNull;

@Component
@Profile(Profiles.DATABASE)
public class AllRolesImpl implements AllRoles {

    // TODO: Shouldn't be hardcoded
    private int companyId = 1;

    private RoleMapper roleMapper;

    @Autowired
    public AllRolesImpl(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }

    @Override
    public List<Role> getAll() {
        return roleMapper.selectAllRoles(companyId);
    }

    @Override
    public Role get(int id) {
        return roleMapper.selectRoleById(id, companyId);
    }

    @Override
    public Role add(Role role) {
        roleMapper.insertRole(role, companyId);
        return role;
    }

    @Override
    public boolean contains(Set<String> roleNames) {
        if(isNull(roleNames) || roleNames.isEmpty()) {
            return false;
        }

        Set<String> existingMatchingNames = roleMapper.retrieveMatchingNames(roleNames, companyId);

        return roleNames.equals(existingMatchingNames);
    }

    @Override
    public boolean delete(int id){
        return roleMapper.deleteRole(id, companyId);
    }

    @Override
    public boolean update(Role role) {
        return roleMapper.updateRole(role, companyId);
    }
}
