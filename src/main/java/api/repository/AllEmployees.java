package api.repository;

import api.domain.Employee;

import java.util.List;

// http://philcalcado.com/2010/12/23/how_to_write_a_repository.html
// Naming based on the above article
public interface AllEmployees {

    List<Employee> getAll();

    Employee get(int id);

    Employee add(Employee employee);

    boolean delete(int id);

    boolean contains(List<String> employeeNames);

    Employee update(Employee employee);

}
