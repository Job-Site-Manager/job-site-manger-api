package api.repository;

import api.domain.Job;

import java.util.List;

public interface AllJobs {

     List<Job> getAll();

     Job get(int id);

     Job add(Job job);

     Job update(Job job);

     boolean delete(int id);

     boolean contains(String jobName);

}
