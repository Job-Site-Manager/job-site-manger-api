package api.repository;

import api.config.Profiles;
import api.domain.Employee;
import api.domain.Role;
import api.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static api.util.ListUtil.areListsEqual;
import static java.util.Objects.isNull;

@Component
@Profile(Profiles.DATABASE)
public class AllEmployeesImpl implements AllEmployees {

    // TODO: Shouldn't be hardcoded
    private int companyId = 1;

    private EmployeeMapper employeeMapper;
    private AllRoles allRoles;

    @Autowired
    public AllEmployeesImpl(EmployeeMapper employeeMapper, AllRoles allRoles) {
        this.employeeMapper = employeeMapper;
        this.allRoles = allRoles;
    }

    @Override
    public List<Employee> getAll() {
        return employeeMapper.selectAllEmployees(companyId);
    }

    @Override
    public Employee get(int id) {
        return employeeMapper.selectEmployeeById(id, companyId);
    }

    // TODO: Validation Logic should be moved out of here
    // Should be in a validation object
    @Override
    public Employee add(Employee employee) {
        if(contains(Collections.singletonList(employee.getName()))) {
            throw new IllegalStateException("Cannot add employee. Employee with that name already exists");
        }

        Set<String> roleNameSet = employee.getSecondaryRoles().stream().map(Role::getName).collect(Collectors.toSet());

        if(!roleNameSet.add(employee.getPrimaryRole().getName())) {
            throw new IllegalStateException("Cannot add employee. The employees primary role matches a secondary role");
        }

        if(!allRoles.contains(roleNameSet)) {
            throw new IllegalStateException("Cannot add employee. One of the employee's roles is invalid");
        }

        employeeMapper.insertEmployeeWithoutRoles(employee, companyId);
        employeeMapper.insertPrimaryRole(employee.getId(), employee.getPrimaryRole(), companyId);
        employeeMapper.insertSecondaryRoles(employee.getId(), employee.getSecondaryRoles(), companyId);

        return employee;
    }

    public Employee update(Employee employee) {
        employeeMapper.updateEmployeeDetails(employee, companyId);

        employeeMapper.deletePrimaryRole(employee.getId());
        employeeMapper.insertPrimaryRole(employee.getId(), employee.getPrimaryRole(), companyId);

        employeeMapper.deleteSecondaryRoles(employee.getId());
        employeeMapper.insertSecondaryRoles(employee.getId(), employee.getSecondaryRoles(), companyId);

        return employeeMapper.selectEmployeeById(employee.getId(), companyId);
    }

    @Override
    public boolean delete(int id) {
        return employeeMapper.deleteEmployee(id, companyId);
    }

    @Override
    public boolean contains(List<String> employeeNames) {
        if(isNull(employeeNames) || employeeNames.isEmpty()) {
            return false;
        }

        List<String> existingEmployeeNames = employeeMapper.retrieveMatchingNames(employeeNames, companyId);

        return areListsEqual(existingEmployeeNames, employeeNames);
    }

}
