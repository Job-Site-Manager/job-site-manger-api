package api.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static api.util.ListUtil.areListsEqual;
import static org.assertj.core.api.Assertions.*;

public class ListUtilTest {

    @Test
    public void areListsEqual_PassedEqualListsWithSameOrder_ReturnsTrue() {
        assertThat(
            areListsEqual(Arrays.asList("Eric", "Kate", "Ben", "Kate"), Arrays.asList("Eric", "Kate", "Ben", "Kate"))
        ).isTrue();
    }

    @Test
    public void areListsEqual_PassedEqualListsWithDifferentOrder_ReturnsTrue() {
        assertThat(
            areListsEqual(Arrays.asList("Eric", "Kate", "Kate", "Ben"), Arrays.asList("Eric", "Kate", "Ben", "Kate"))
        ).isTrue();
    }

    @Test
    public void areListsEqual_PassedTwoNullLists_ReturnsTrue() {
        assertThat(areListsEqual(null, null)).isTrue();
    }

    @Test
    public void areListsEqual_PassedTwoEmptyLists_ReturnsTrue() {
        assertThat(areListsEqual(Collections.emptyList(), Collections.emptyList())).isTrue();
    }

    @Test
    public void areListsEqual_PassedListOneContainsSmallSubsetOfListTwo_ReturnsFalse() {
        assertThat(
            areListsEqual(Arrays.asList("Eric", "Kate", "Ben", "Ben"), Arrays.asList("Eric", "Kate", "Ben", "Kate"))
        ).isFalse();
    }

    @Test
    public void areListsEqual_PassedListTwoContainsSmallSubsetOfListOne_ReturnsFalse() {
        assertThat(
            areListsEqual(Arrays.asList("Eric", "Kate", "Ben", "Kate"), Arrays.asList("Eric", "Kate", "Ben", "Ben"))
        ).isFalse();
    }

    @Test
    public void areListsEqual_PassedUnequalLists_ReturnsFalse() {
        assertThat(
            areListsEqual(Arrays.asList("Eric", "Kate", "Ben", "Kate"), Arrays.asList("Eric", "Kate", "Ben", "Tony"))
        ).isFalse();
    }

    @Test
    public void areListsEqual_ListOneIsNull_ReturnsFalse() {
        assertThat(areListsEqual(null, Arrays.asList("Eric", "Kate", "Ben", "Tony"))).isFalse();
    }

    @Test
    public void areListsEqual_ListTwoIsNull_ReturnsFalse() {
        assertThat(areListsEqual(Arrays.asList("Eric", "Kate", "Ben", "Tony"), null)).isFalse();
    }
}