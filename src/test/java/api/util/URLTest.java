package api.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class URLTest {

    @Test
    public void replacePlusSignsWithSpaces_StringSeparatedByPlusSigns_ReplacesPlusSignsWithSpaces() throws Exception {
        assertThat(
            URL.replacePlusSignsWithSpaces("this+is+a+test+string")
        ).isEqualTo("this is a test string");
    }

}