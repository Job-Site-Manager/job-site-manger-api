package api.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

public class JobTest {

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Job.class).usingGetClass().suppress(Warning.NONFINAL_FIELDS).verify();
    }

}