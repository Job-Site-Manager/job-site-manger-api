package api.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

public class JobPositionTest {

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(JobPosition.class).usingGetClass().suppress(Warning.NONFINAL_FIELDS).verify();
    }

}