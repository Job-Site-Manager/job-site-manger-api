package api.domain.json.mixins;

import api.domain.Employee;
import api.domain.Role;
import api.domain.data.CompanyOne;
import api.domain.json.mixins.helpers.JsonReader;
import api.domain.json.mixins.helpers.ObjectMapperFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import test_utilities.CustomTestAsserts;

import static org.assertj.core.api.Assertions.*;

public class EmployeeMixinTest {

    private ObjectMapper objectMapper;
    private Employee tomHouston;
    private String tomHoustonJson;

    public EmployeeMixinTest() throws Exception{
        objectMapper = ObjectMapperFactory.buildMapper();

        objectMapper
            .addMixIn(Employee.class, EmployeeMixin.class)
            .addMixIn(Role.class, RoleMixin.class);

        tomHouston = CompanyOne.EmployeeData.tomHouston;
        tomHoustonJson = JsonReader.readJsonFileInAsAString(getClass().getResource(
            "/json/data/company_one/employee/tomHouston.json"
        ));
    }

    @Test
    public void deserialize_EmployeeObject_CreatesEmployeeJson() throws Exception {
        String actualJson = objectMapper.writeValueAsString(tomHouston);

        CustomTestAsserts.assertActualJsonEqualsExpectedJsonInLenientMode(actualJson, tomHoustonJson);
    }

    @Test
    public void serialize_EmployeeJson_CreatesEmployeeObject() throws Exception {
        Employee actualEmployee = objectMapper.readValue(tomHoustonJson, Employee.class);

        assertThat(actualEmployee).isEqualTo(tomHouston);
    }

}
