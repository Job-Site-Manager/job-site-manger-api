package api.domain.json.mixins;

import api.domain.Role;
import api.domain.data.CompanyOne;
import api.domain.json.mixins.helpers.JsonReader;
import api.domain.json.mixins.helpers.ObjectMapperFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import test_utilities.CustomTestAsserts;

import static org.assertj.core.api.Assertions.*;

public class RoleMixinTest {

    private ObjectMapper objectMapper;
    private Role bulldozerOperator;
    private String bulldozerOperatorJson;

    public RoleMixinTest() throws Exception {
        objectMapper = ObjectMapperFactory.buildMapper();
        objectMapper.addMixIn(Role.class, RoleMixin.class);

        bulldozerOperator = CompanyOne.RoleData.bulldozerOperator;
        bulldozerOperatorJson = JsonReader.readJsonFileInAsAString(getClass().getResource(
            "/json/data/company_one/role/bulldozerOperator.json"
        ));
    }

    @Test
    public void deserialize_RoleObject_CreatesRoleJson() throws Exception {
        String actualJson = objectMapper.writeValueAsString(bulldozerOperator);

        CustomTestAsserts.assertActualJsonEqualsExpectedJson(actualJson, bulldozerOperatorJson);
    }

    @Test
    public void serialize_RoleJson_CreatesRoleObject() throws Exception {
        Role actualRole = objectMapper.readValue(bulldozerOperatorJson, Role.class);

        assertThat(actualRole).isEqualTo(bulldozerOperator);
    }

}
