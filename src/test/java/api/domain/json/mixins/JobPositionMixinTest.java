package api.domain.json.mixins;

import api.domain.Employee;
import api.domain.JobPosition;
import api.domain.Role;
import api.domain.data.CompanyOne;
import api.domain.json.mixins.helpers.JsonReader;
import api.domain.json.mixins.helpers.ObjectMapperFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import test_utilities.CustomTestAsserts;

import static org.assertj.core.api.Assertions.assertThat;

public class JobPositionMixinTest {

    private ObjectMapper objectMapper;
    private JobPosition bulldozerOperatorAtTheOldPond;
    private String bulldozerOperatorAtTheOldPondJson;

    public JobPositionMixinTest() throws Exception{
        objectMapper = ObjectMapperFactory.buildMapper();
        objectMapper
            .addMixIn(JobPosition.class, JobPositionMixin.class)
            .addMixIn(Employee.class, EmployeeMixin.class)
            .addMixIn(Role.class, RoleMixin.class);

        bulldozerOperatorAtTheOldPond = CompanyOne.JobPositionData.bulldozerOperatorAtTheOldPond;
        bulldozerOperatorAtTheOldPondJson = JsonReader.readJsonFileInAsAString(getClass().getResource(
            "/json/data/company_one/job_position/bulldozerOperatorAtTheOldPond.json"
        ));
    }

    @Test
    public void serialize_JobPositionObject_CreatesJobPositionJson() throws Exception {
        String actualJson = objectMapper.writeValueAsString(bulldozerOperatorAtTheOldPond);

        CustomTestAsserts.assertActualJsonEqualsExpectedJsonInLenientMode(
            actualJson, bulldozerOperatorAtTheOldPondJson
        );
    }

    @Test
    public void deserialize_JobPositionJson_CreatesJobPositionObject() throws Exception {
        JobPosition actualEmployeeForRole = objectMapper.readValue(bulldozerOperatorAtTheOldPondJson, JobPosition.class);

        assertThat(actualEmployeeForRole).isEqualTo(bulldozerOperatorAtTheOldPond);
    }

}
