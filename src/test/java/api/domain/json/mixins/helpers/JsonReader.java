package api.domain.json.mixins.helpers;

import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.net.URL;

public class JsonReader {

    public static String readJsonFileInAsAString(URL url) throws Exception {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(url.getPath()));

        return obj.toString();
    }

}
