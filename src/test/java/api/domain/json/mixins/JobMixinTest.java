package api.domain.json.mixins;

import api.domain.*;
import api.domain.data.CompanyOne;
import api.domain.json.mixins.helpers.JsonReader;
import api.domain.json.mixins.helpers.ObjectMapperFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import test_utilities.CustomTestAsserts;

import static org.assertj.core.api.Assertions.assertThat;

public class JobMixinTest  {

    private ObjectMapper objectMapper;
    private Job theOldPond;
    private String theOldPondJson;

    public JobMixinTest() throws Exception{
        objectMapper = ObjectMapperFactory.buildMapper();
        objectMapper
            .addMixIn(Job.class, JobMixin.class)
            .addMixIn(JobPosition.class, JobPositionMixin.class)
            .addMixIn(Employee.class, EmployeeMixin.class)
            .addMixIn(Role.class, RoleMixin.class);

        theOldPond = CompanyOne.JobData.theOldPond;
        theOldPondJson = JsonReader.readJsonFileInAsAString(getClass().getResource(
            "/json/data/company_one/job/theOldPond.json"
        ));
    }

    @Test
    public void deserialize_JobObject_CreatesJobJson() throws Exception {
        String actualJson = objectMapper.writeValueAsString(theOldPond);

        CustomTestAsserts.assertActualJsonEqualsExpectedJsonInLenientMode(actualJson, theOldPondJson);
    }

    @Test
    public void serialize_JobJson_CreatesJobObject() throws Exception {
        Job actualJob = objectMapper.readValue(theOldPondJson, Job.class);

        assertThat(actualJob).isEqualTo(theOldPond);
    }

}
