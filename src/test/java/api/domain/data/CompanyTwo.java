package api.domain.data;

import api.domain.*;

import java.util.*;

public class CompanyTwo {

    public static final int id = 2;

    public static class RoleData {

        public static Role carpenter = new Role("Carpenter", "Needs a hammer", 6);
        public static Role plumber = new Role("Plumber", null, 7);

        public static List<Role> allRoles = new ArrayList<>(Arrays.asList(carpenter, plumber));

    }

    public static class EmployeeData {

        public static Employee billHader = new Employee(
            9, "Bill Hader", "23 Dundas St.", "613-323-5433",
            RoleData.carpenter,
            new HashSet<>(Collections.singletonList(RoleData.plumber))
        );
        public static Employee tomStropingly = new Employee(
            10, "Tom Stropingly", "12 Left Lane", "432-342-2423",
            RoleData.plumber,
            new HashSet<>(Collections.singletonList(RoleData.carpenter))
        );

        public static List<Employee> allEmployees = new ArrayList<>(Arrays.asList(billHader, tomStropingly));

    }

    public static class JobPositionData {

        public static JobPosition carpenterAtTheBigHouse = new JobPosition(
            9, RoleData.carpenter, 2,
            new HashSet<>(Arrays.asList(EmployeeData.billHader))
        );
        public static JobPosition plumberAtTheBigHouse = new JobPosition(
            10, RoleData.plumber, 1,
            new HashSet<>(Arrays.asList(EmployeeData.tomStropingly))
        );

    }

    public static class JobPositionsData {

        public static Set<JobPosition> theBigHouse = new HashSet<>(
            Arrays.asList(JobPositionData.carpenterAtTheBigHouse, JobPositionData.plumberAtTheBigHouse)
        );

    }

    public static class JobData {

        public static Job theBigHouse = new Job(
            3, "The Big House", "425 Talbot  Blvd.", JobPositionsData.theBigHouse
        );

    }

}
