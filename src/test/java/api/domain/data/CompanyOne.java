package api.domain.data;

import api.domain.*;

import java.util.*;

public class CompanyOne {

    public static final int id = 1;

    public static class RoleData {

        public static Role bulldozerOperator = new Role("Bulldozer Operator", "Requires a bulldozer", 1);
        public static Role backhoeOperator = new Role("Backhoe Operator", "Requires a backhoe", 2);
        public static Role rockTruckOperator = new Role("Rock Truck Operator", null, 3);
        public static Role scraperOperator = new Role("Scraper Operator", "Driver should not have a sore back", 4);
        public static Role packerOperator = new Role("Packer Operator", "Requires a packer", 5);
        public static Role labourer = new Role("Labourer", "Needs to be in good shape", 8);

        public static List<Role> allRoles = new ArrayList<>(Arrays.asList(
            bulldozerOperator, backhoeOperator, rockTruckOperator, scraperOperator, packerOperator, labourer
        ));

    }

    public static class EmployeeData {

        public static Employee tomHouston = new Employee(
            1, "Tom Houston", "12 Steep Road", "519-425-6434",
            RoleData.bulldozerOperator,
            new HashSet<>(Arrays.asList(RoleData.backhoeOperator, RoleData.scraperOperator))
        );
        public static Employee billCrosby = new Employee(
            2, "Bill Crosby", "23 Rocky Road", "519-734-9921",
            RoleData.backhoeOperator,
            new HashSet<>(Arrays.asList(RoleData.rockTruckOperator, RoleData.scraperOperator, RoleData.packerOperator))
        );
        public static Employee wayneGretzky = new Employee(
            3, "Wayne Gretzky", "99 Greztky Lane", "519-528-1884",
            RoleData.scraperOperator,
            new HashSet<>()
        );
        public static Employee timJefferson = new Employee(
            4, "Tim Jefferson", "12 Smoke Cresent", "519-625-6434",
            RoleData.rockTruckOperator,
            new HashSet<>(Collections.singletonList(RoleData.backhoeOperator))
        );
        public static Employee susanMoore = new Employee(
            5, "Susan Moore", "1 First Road", "519-625-6774",
            RoleData.scraperOperator,
            new HashSet<>(Arrays.asList(RoleData.backhoeOperator, RoleData.rockTruckOperator))
        );
        public static Employee tomRosen = new Employee(
            6, "Tom Rosen", "12 Half Road", "519-929-6484",
            RoleData.bulldozerOperator,
            new HashSet<>(Arrays.asList(RoleData.scraperOperator, RoleData.packerOperator))
        );
        public static Employee austonMatthews = new Employee(
            7, "Auston Matthews", "7843 Dundas Steet", "519-725-6494",
            RoleData.packerOperator,
            new HashSet<>(Collections.singletonList(RoleData.backhoeOperator))
        );
        public static Employee mitchMarner = new Employee(
            8, "Mitch Marner", "12 Steep Road", "519-625-6434",
            RoleData.packerOperator,
            new HashSet<>(Collections.singletonList(RoleData.backhoeOperator))
        );

        public static List<Employee> allEmployees = new ArrayList<>(Arrays.asList(
            tomHouston,
            billCrosby,
            wayneGretzky,
            timJefferson,
            susanMoore,
            tomRosen,
            austonMatthews,
            mitchMarner
        ));

    }

    public static class JobPositionData {

        public static int bulldozerOperatorAtTheOldPondId = 1;

        public static JobPosition bulldozerOperatorAtTheOldPond = new JobPosition(
            1, RoleData.bulldozerOperator, 2,
            new HashSet<>(Arrays.asList(EmployeeData.tomHouston))
        );
        public static JobPosition backhoeOperatorAtTheOldPond = new JobPosition(
            2, RoleData.backhoeOperator, 1,
            new HashSet<>(Arrays.asList(EmployeeData.billCrosby))
        );
        public static JobPosition rockTruckOperatorAtTheOldPond = new JobPosition(
            3, RoleData.rockTruckOperator, 2,
            new HashSet<>(Arrays.asList(EmployeeData.susanMoore, EmployeeData.timJefferson))
        );
        public static JobPosition packerOperatorAtTheGravelPit = new JobPosition(
            4, RoleData.packerOperator, 2,
            new HashSet<>(Arrays.asList(EmployeeData.austonMatthews, EmployeeData.mitchMarner))
        );
        public static JobPosition scraperOperatorAtTheGravelPit = new JobPosition(
            5, RoleData.scraperOperator, 1,
            new HashSet<>(Arrays.asList(EmployeeData.wayneGretzky))
        );
        public static JobPosition bulldozerOperatorAtTheGravelPit = new JobPosition(
            6, RoleData.bulldozerOperator, 1,
            new HashSet<>(Arrays.asList(EmployeeData.tomRosen))
        );

    }

    public static class JobPositionsData {

        public static Set<JobPosition> theOldPond = new HashSet<>(Arrays.asList(
            JobPositionData.backhoeOperatorAtTheOldPond,
            JobPositionData.bulldozerOperatorAtTheOldPond,
            JobPositionData.rockTruckOperatorAtTheOldPond
        ));


        public static Set<JobPosition> gravelPit = new HashSet<>(Arrays.asList(
            JobPositionData.bulldozerOperatorAtTheGravelPit,
            JobPositionData.packerOperatorAtTheGravelPit,
            JobPositionData.scraperOperatorAtTheGravelPit
        ));

    }

    public static class JobData {
        public static List<Employee> theOldPondAssignedEmployees = createTheOldPondAssignedEmployeesList();
        public static List<Role> theOldPondAssignedRoles = createTheOldPondAssignedRolesList();

        public static Job theOldPond = new Job(1, "The Old Pond", "23 Pond Rd.", JobPositionsData.theOldPond);

        public static Job gravelPit = new Job(2, "Gravel Pit", "434 Beach Rd.", JobPositionsData.gravelPit);

        public static List<Job> allJobs = new ArrayList<>(Arrays.asList(theOldPond, gravelPit));

        private static List<Employee> createTheOldPondAssignedEmployeesList() {
            List<Employee> theOldPondAssignedEmployees = new ArrayList<>();

            JobPositionsData.theOldPond.forEach(jobPosition ->
                theOldPondAssignedEmployees.addAll(jobPosition.getEmployeeSet())
            );

            return theOldPondAssignedEmployees;
        }

        private static List<Role> createTheOldPondAssignedRolesList() {
            List<Role> theOldPondAssignedRoles = new ArrayList<>();

            JobPositionsData.theOldPond.forEach(jobPosition ->
                theOldPondAssignedRoles.add(jobPosition.getRole())
            );

            return theOldPondAssignedRoles;
        }
    }

}
