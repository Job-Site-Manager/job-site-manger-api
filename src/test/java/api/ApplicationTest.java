package api;

import api.config.Profiles;
import api.config.TestProfiles;
import api.controller.EmployeeController;
import api.controller.JobController;
import api.controller.RoleController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = {Profiles.DATABASE, TestProfiles.MOCK_DATA_SOURCE })
@SpringBootTest
public class ApplicationTest {

    @Autowired
    private ApplicationContext appContext;

    @Autowired
    EmployeeController employeeController;

    @Autowired
    JobController jobController;

    @Autowired
    RoleController roleController;

    @Test
    public void contextLoads() throws Exception {
        assertThat(employeeController).isNotNull();
        assertThat(jobController).isNotNull();
        assertThat(roleController).isNotNull();
    }

}
