package api.repository;

import api.domain.Employee;
import api.domain.Role;
import api.domain.data.CompanyOne;
import api.mapper.EmployeeMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AllEmployeesImplTest {

    private AllEmployeesImpl allEmployeesImpl;
    private EmployeeMapper employeeMapper;
    private AllRoles allRoles;
    private int companyId = 1;

    @Before
    public void setup() {
        employeeMapper = mock(EmployeeMapper.class);
        allRoles = mock(AllRoles.class);
        allEmployeesImpl = new AllEmployeesImpl(employeeMapper, allRoles);
    }

    @Test
    public void add_NewEmployee_CorrectlyAddsEmployee() {
        Role primaryRole = CompanyOne.RoleData.rockTruckOperator;
        Set<Role> secondaryRoles = new HashSet<>(Arrays.asList(
            CompanyOne.RoleData.scraperOperator, CompanyOne.RoleData.packerOperator
        ));
        Employee newEmployee = new Employee(
            12, "Tom Tiger", "123 Street", "519-451-7306", primaryRole, secondaryRoles
        );

        when(employeeMapper.retrieveMatchingNames(
            Collections.singletonList(newEmployee.getName()), companyId
        )).thenReturn(Collections.emptyList());
        when(allRoles.contains(
            new HashSet<>(Arrays.asList(
                primaryRole.getName(),
                CompanyOne.RoleData.scraperOperator.getName(),
                CompanyOne.RoleData.packerOperator.getName()
            ))
        )).thenReturn(true);

        allEmployeesImpl.add(newEmployee);

        verify(employeeMapper).insertEmployeeWithoutRoles(newEmployee, companyId);
        verify(employeeMapper).insertPrimaryRole(newEmployee.getId(), primaryRole, companyId);
        verify(employeeMapper).insertSecondaryRoles(newEmployee.getId(), secondaryRoles, companyId);
    }

    @Test(expected = IllegalStateException.class)
    public void add_EmployeeThatAlreadyExists_ThrowsException() {
        Employee newEmployee = new Employee(
            12, "Tom Tiger", "123 Street", "519-451-7306",
            null,
            new HashSet<>()
        );

        when(employeeMapper.retrieveMatchingNames(
            Collections.singletonList(newEmployee.getName()), companyId
        )).thenReturn(Collections.singletonList(newEmployee.getName()));

        allEmployeesImpl.add(newEmployee);
    }

    @Test(expected = IllegalStateException.class)
    public void add_EmployeeHasSamePrimaryAndSecondaryRole_ThrowsException() {
        Employee newEmployee = new Employee(
            12, "Tom Tiger", "123 Street", "519-451-7306",
            CompanyOne.RoleData.rockTruckOperator,
            new HashSet<>(Collections.singletonList(CompanyOne.RoleData.rockTruckOperator))
        );

        when(employeeMapper.retrieveMatchingNames(
            Collections.singletonList(newEmployee.getName()), companyId
        )).thenReturn(Collections.emptyList());
        when(allRoles.contains(any())).thenReturn(true);

        allEmployeesImpl.add(newEmployee);
    }

    @Test(expected = IllegalStateException.class)
    public void add_EmployeesContainsInvalidRole_ThrowsException() {
        Employee newEmployee = new Employee(
            12, "Tom Tiger", "123 Street", "519-451-7306",
            CompanyOne.RoleData.rockTruckOperator,
            new HashSet<>(Arrays.asList(
                new Role("Unknown", null, 20), CompanyOne.RoleData.packerOperator
            ))
        );

        when(employeeMapper.retrieveMatchingNames(
            Collections.singletonList(newEmployee.getName()), companyId
        )).thenReturn(Collections.emptyList());
        when(allRoles.contains(any())).thenReturn(false);

        allEmployeesImpl.add(newEmployee);
    }

    @Test
    public void contains_PassedNullList_ReturnsFalse() {
        assertThat(allEmployeesImpl.contains(null)).isFalse();
    }

    @Test
    public void contains_PassedEmptyList_ReturnsFalse() {
        assertThat(allEmployeesImpl.contains(Collections.emptyList())).isFalse();
    }

    @Test
    public void contains_AllNamesExist_ReturnsTrue() {
        List<String> nameList = Arrays.asList("Eric Lammers", "Kate Weverink", "Tony Romo");

        when(employeeMapper.retrieveMatchingNames(any(), anyInt())).thenReturn(nameList);

        assertThat(allEmployeesImpl.contains(nameList)).isTrue();
    }

    @Test
    public void contains_OnlySomeNamesFound_ReturnsFalse() {
        String eric = "Eric Lammers";
        String kate = "Kate Weverink";

        when(employeeMapper.retrieveMatchingNames(any(), anyInt())).thenReturn(Arrays.asList(eric, kate));

        assertThat(allEmployeesImpl.contains(Arrays.asList(eric, kate, "Tony Romo"))).isFalse();
    }

    @Test
    public void contains_NoneOfTheNamesFound_ReturnsFalse() {
        when(employeeMapper.retrieveMatchingNames(any(), anyInt())).thenReturn(Collections.emptyList());

        assertThat(allEmployeesImpl.contains(Arrays.asList("Eric Lammers", "Kate Weverink", "Tony Romo"))).isFalse();
    }
}