package api.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;


@Profile(TestProfiles.MOCK_DATA_SOURCE)
@Configuration
public class MockDataSourceConfig {

    @Bean
    @Primary
    public DataSource dataSource() {
        return Mockito.mock(DataSource.class);
    }

}
