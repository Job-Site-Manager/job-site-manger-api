package api.controller;

import api.config.Profiles;
import api.config.TestProfiles;
import api.domain.data.CompanyOne;
import api.domain.json.mixins.helpers.JsonReader;
import api.service.EmployeeService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import test_utilities.CustomTestAsserts;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = { Profiles.DATABASE, TestProfiles.MOCK_DATA_SOURCE })
@SpringBootTest
public class EmployeeControllerTest {

    private MockMvc mvc;
    @Mock private EmployeeService employeeService;
    @InjectMocks private EmployeeController employeeController;
    @Autowired private Jackson2ObjectMapperBuilder mapperBuilder;

    @Before
    public void setup() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(mapperBuilder.build());

        mvc = standaloneSetup(employeeController).setMessageConverters(converter).build();
    }

    @Test
    public void getAllEmployees_CompanyHasEmployees_ResponseContainsTheListOfEmployees() throws Exception {
        Mockito.when(employeeService.getAllEmployees()).thenReturn(CompanyOne.EmployeeData.allEmployees);

        MockHttpServletResponse response = mvc.perform(
            get("/employees").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        String expectedJsonOfAllEmployees = readInJson("/json/data/company_one/employee/allEmployees.json");

        CustomTestAsserts.assertActualJsonEqualsExpectedJsonInLenientMode(
            response.getContentAsString(), expectedJsonOfAllEmployees
        );
    }

    @Test
    public void getAllEmployees_CompanyHasEmployees_ReturnsHttpStatusOK() throws Exception {
        Mockito.when(employeeService.getAllEmployees()).thenReturn(CompanyOne.EmployeeData.allEmployees);

        MockHttpServletResponse response = mvc.perform(
            get("/employees").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getAllEmployees_NoEmployeesInTheCompany_ResponseContainsAnEmptyList() throws Exception {
        Mockito.when(employeeService.getAllEmployees()).thenReturn(Collections.emptyList());

        MockHttpServletResponse response = mvc.perform(
            get("/employees").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        CustomTestAsserts.assertActualJsonEqualsExpectedJson(response.getContentAsString(), "[]");
    }

    @Test
    public void getAllEmployees_NoEmployeesInTheCompany_ReturnsHttpStatusOK() throws Exception {
        Mockito.when(employeeService.getAllEmployees()).thenReturn(Collections.emptyList());

        MockHttpServletResponse response = mvc.perform(
            get("/employees").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getEmployeeById_PassedAValidEmployeeId_ResponseContainsTheEmployee() throws Exception {
        Mockito.when(employeeService.getEmployee(CompanyOne.EmployeeData.tomHouston.getId()))
            .thenReturn(CompanyOne.EmployeeData.tomHouston);

        MockHttpServletResponse response = mvc.perform(
            get("/employee/"  + CompanyOne.EmployeeData.tomHouston.getId()).accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        String expectedJsonForEmployeeTomHouston = readInJson("/json/data/company_one/employee/tomHouston.json");

        CustomTestAsserts.assertActualJsonEqualsExpectedJsonInLenientMode(
            response.getContentAsString(), expectedJsonForEmployeeTomHouston
        );
    }

    @Test
    public void getEmployeeById_PassedAValidEmployeeId_ReturnsHttpStatusOK() throws Exception {
        Mockito.when(employeeService.getEmployee(CompanyOne.EmployeeData.tomHouston.getId()))
            .thenReturn(CompanyOne.EmployeeData.tomHouston);

        MockHttpServletResponse response = mvc.perform(
            get("/employee/" + CompanyOne.EmployeeData.tomHouston.getId()).accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getEmployeeById_IdDoesNotMatchAnyEmployee_ResponseContainsAnEmptyBody() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            get("/employee/999999").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getContentAsString()).isEmpty();
    }

    @Ignore("Ignored till I look into http responses with Spring")
    @Test
    public void getEmployeeById_IdDoesNotMatchAnyEmployee_ReturnsHttpStatusNOTFOUND() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            get("/employee/999").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void addEmployee_PassedAValidEmployee_CallsTheServiceToAddTheEmployee() throws Exception {
        mvc.perform(
            post("/employee").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/employee/tomHouston.json")
            )
        ).andReturn().getResponse();

        Mockito.verify(employeeService).addEmployee(CompanyOne.EmployeeData.tomHouston);
    }

    @Ignore("Ignored till I look into http responses with Spring")
    @Test
    public void addEmployee_PassedAValidEmployee_ReturnsHttpStatusCREATED() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            post("/employee").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/employee/tomHouston.json")
            )
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addEmployee_EmployeeAlreadyExistsInTheCompany_ReturnsHttpStatusCONFLICT() throws Exception {
        Mockito.when(employeeService.addEmployee(any())).thenThrow(new IllegalStateException());

        MockHttpServletResponse response = mvc.perform(
            post("/employee").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/employee/tomHouston.json")
            )
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addEmployee_PassedInvalidJson_ReturnsHttpStatusBADREQUEST() throws Exception {
        throw new Exception("This test needs to be implemented");
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addEmployee_PassedInvalidJson_ReturnsTheCorrectErrorMessage() throws Exception {
        throw new Exception("This test needs to be implemented");
    }

    private String readInJson(String path) throws Exception {
        return JsonReader.readJsonFileInAsAString(getClass().getResource(path));
    }

}