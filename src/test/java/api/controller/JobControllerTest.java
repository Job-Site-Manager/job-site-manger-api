package api.controller;

import api.config.Profiles;
import api.config.TestProfiles;
import api.domain.data.CompanyOne;
import api.domain.json.mixins.helpers.JsonReader;
import api.service.JobService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import test_utilities.CustomTestAsserts;
import java.util.Collections;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = { Profiles.DATABASE, TestProfiles.MOCK_DATA_SOURCE })
@SpringBootTest
public class JobControllerTest {

    private MockMvc mvc;
    @Mock private JobService jobService;
    @InjectMocks private JobController jobController;
    @Autowired private Jackson2ObjectMapperBuilder mapperBuilder;

    @Before
    public void setup() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(mapperBuilder.build());

        mvc = standaloneSetup(jobController).setMessageConverters(converter).build();
    }

    @Test
    public void getAllJobs_CompanyHasJobs_ResponseContainsTheListOfJobs() throws Exception {
        Mockito.when(jobService.getAllJobs()).thenReturn(CompanyOne.JobData.allJobs);

        MockHttpServletResponse response = mvc.perform(
            get("/jobs").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        String expectedJsonOfAllJobs = readInJson("/json/data/company_one/job/allJobs.json");

        CustomTestAsserts.assertActualJsonEqualsExpectedJsonInLenientMode(
            response.getContentAsString(), expectedJsonOfAllJobs
        );
    }

    @Test
    public void getAllJobs_CompanyHasJobs_ReturnsHttpStatusOK() throws Exception {
        Mockito.when(jobService.getAllJobs()).thenReturn(CompanyOne.JobData.allJobs);

        MockHttpServletResponse response = mvc.perform(
            get("/jobs").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getAllJobs_CurrentlyNoJobsForTheCompany_ResponseContainsAnEmptyList() throws Exception {
        Mockito.when(jobService.getAllJobs()).thenReturn(Collections.emptyList());

        MockHttpServletResponse response = mvc.perform(
            get("/jobs").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        CustomTestAsserts.assertActualJsonEqualsExpectedJson(response.getContentAsString(), "[]");
    }

    @Test
    public void getAllJobs_CurrentlyNoJobsForTheCompany_ReturnsHttpStatusOK() throws Exception {
        Mockito.when(jobService.getAllJobs()).thenReturn(Collections.emptyList());

        MockHttpServletResponse response = mvc.perform(
            get("/jobs").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getJobById_PassedAValidJobId_ResponseContainsTheJob() throws Exception {
        Mockito.when(jobService.getJob(CompanyOne.JobData.theOldPond.getId())).thenReturn(CompanyOne.JobData.theOldPond);

        MockHttpServletResponse response = mvc.perform(
            get("/job/1").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        String expectedJsonForTheOldPondJob = readInJson("/json/data/company_one/job/theOldPond.json");

        CustomTestAsserts.assertActualJsonEqualsExpectedJsonInLenientMode(
            response.getContentAsString(), expectedJsonForTheOldPondJob
        );
    }

    @Test
    public void getJobById_PassedAValidJobId_ReturnsHttpStatusOK() throws Exception {
        int jobId = 1;
        Mockito.when(jobService.getJob(jobId)).thenReturn(CompanyOne.JobData.theOldPond);

        MockHttpServletResponse response = mvc.perform(
            get("/job/" + jobId).accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getJobById_IdDoesNotMatchAnyJob_ResponseContainsAnEmptyBody() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            get("/job/99999").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getContentAsString()).isEmpty();
    }

    @Ignore("Ignored till I look into http responses with Spring")
    @Test
    public void getJobById_IdDoesNotMatchAnyJob_ReturnsHttpStatusNOTFOUND() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            get("/job/9999").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void addJob_PassedAValidJob_CallsTheServiceToAddTheJob() throws Exception {
        mvc.perform(
            post("/job").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/job/theOldPond.json")
            )
        ).andReturn().getResponse();

        Mockito.verify(jobService).addJob(CompanyOne.JobData.theOldPond);
    }

    @Ignore("Ignored till I look into http responses with Spring")
    @Test
    public void addJob_PassedAValidJob_ReturnsHttpStatusCREATED() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            post("/job").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/job/theOldPond.json")
            )
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addJob_JobAlreadyExistsInTheCompany_ReturnsHttpStatusCONFLICT() throws Exception {
        Mockito.doThrow(new IllegalStateException()).when(jobService).addJob(any());

        MockHttpServletResponse response = mvc.perform(
            post("/job").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/job/theOldPond.json")
            )
        ).andReturn().getResponse();

        Mockito.verify(jobService).addJob(CompanyOne.JobData.theOldPond);
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addJob_PassedInvalidJson_ReturnsHttpStatusBADREQUEST() throws Exception {
        throw new Exception("This test needs to be implemented");
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addJob_PassedInvalidJson_ReturnsTheCorrectErrorMessage() throws Exception {
        throw new Exception("This test needs to be implemented");
    }

    private String readInJson(String path) throws Exception {
        return JsonReader.readJsonFileInAsAString(getClass().getResource(path));
    }

}