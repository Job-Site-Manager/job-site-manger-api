package api.controller;

import api.config.Profiles;
import api.config.TestProfiles;
import api.domain.data.CompanyOne;
import api.domain.json.mixins.helpers.JsonReader;
import api.service.RoleService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import test_utilities.CustomTestAsserts;

import java.util.Collections;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = { Profiles.DATABASE, TestProfiles.MOCK_DATA_SOURCE })
@SpringBootTest
public class RoleControllerTest {

    private MockMvc mvc;
    @Mock private RoleService roleService;
    @InjectMocks private RoleController roleController;
    @Autowired private Jackson2ObjectMapperBuilder mapperBuilder;

    @Before
    public void setup() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(mapperBuilder.build());

        mvc = standaloneSetup(roleController).setMessageConverters(converter).build();
    }

    @Test
    public void getAllRoles_TheCompanyHasRolesDefined_ResponseContainsTheListOfRoles() throws Exception {
        Mockito.when(roleService.getAllRoles()).thenReturn(CompanyOne.RoleData.allRoles);

        MockHttpServletResponse response = mvc.perform(
            get("/roles").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        String allRolesJson = readInJson("/json/data/company_one/role/allRoles.json");

        CustomTestAsserts.assertActualJsonEqualsExpectedJson(response.getContentAsString(), allRolesJson);
    }

    @Test
    public void getAllRoles_TheCompanyHasRolesDefined_ReturnsHttpStatusOK() throws Exception {
        Mockito.when(roleService.getAllRoles()).thenReturn(CompanyOne.RoleData.allRoles);

        MockHttpServletResponse response = mvc.perform(
            get("/roles").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getAllRoles_NoRolesExistForTheCompany_ResponseContainsAnEmptyList() throws Exception {
        Mockito.when(roleService.getAllRoles()).thenReturn(Collections.emptyList());

        MockHttpServletResponse response = mvc.perform(
            get("/roles").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        CustomTestAsserts.assertActualJsonEqualsExpectedJson(response.getContentAsString(), "[]");
    }

    @Test
    public void getAllRoles_NoRolesExistForTheCompany_ReturnsHttpStatusOK() throws Exception {
        Mockito.when(roleService.getAllRoles()).thenReturn(Collections.emptyList());

        MockHttpServletResponse response = mvc.perform(
            get("/roles").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getRoleById_PassedAValidRoleId_ResponseContainsTheRole() throws Exception {
        Mockito.when(roleService.getRole(1)).thenReturn(CompanyOne.RoleData.bulldozerOperator);

        MockHttpServletResponse response = mvc.perform(
            get("/role/1").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        String expectedJsonForTheBulldozerOperatorRole = readInJson("/json/data/company_one/role/bulldozerOperator.json");

        CustomTestAsserts.assertActualJsonEqualsExpectedJson(response.getContentAsString(), expectedJsonForTheBulldozerOperatorRole);
    }

    @Test
    public void getRoleById_PassedAValidRoleId_ReturnsHttpStatusOK() throws Exception {
        Mockito.when(roleService.getRole(1)).thenReturn(CompanyOne.RoleData.bulldozerOperator);

        MockHttpServletResponse response = mvc.perform(
            get("/role/1").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getRoleById_IdDoesNotMatchAnyRole_ResponseContainsAnEmptyBody() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            get("/role/9999").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getContentAsString()).isEmpty();
    }

    @Ignore("Ignored till I look into http responses with Spring")
    @Test
    public void getRoleById_IdDoesNotMatchAnyRole_ReturnsHttpStatusNOTFOUND() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            get("/role/unknown+name").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(response.getContentAsString()).isEmpty();
    }

    @Test
    public void addRole_PassedAValidRole_CallsTheServiceToAddTheRole() throws Exception {
        mvc.perform(
            post("/role").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/role/bulldozerOperator.json")
            )
        ).andReturn().getResponse();

        Mockito.verify(roleService).addRole(CompanyOne.RoleData.bulldozerOperator);
    }

    @Ignore("Ignored till I look into http responses with Spring")
    @Test
    public void addRole_PassedAValidRole_ReturnsHttpStatusCREATED() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            post("/role").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/role/bulldozerOperator.json")
            )
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addRole_RoleAlreadyExistsInTheCompany_ReturnsHttpStatusCONFLICT() throws Exception {
        Mockito.when(roleService.addRole(any())).thenThrow(new IllegalStateException());

        MockHttpServletResponse response = mvc.perform(
            post("/role").contentType(MediaType.APPLICATION_JSON).content(
                readInJson("/json/data/company_one/role/bulldozerOperator.json")
            )
        ).andReturn().getResponse();

        Mockito.verify(roleService).addRole(CompanyOne.RoleData.bulldozerOperator);
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addRole_PassedInvalidJson_ReturnsHttpStatusBADREQUEST() throws Exception {
        throw new Exception("This test needs to be implemented");
    }

    @Ignore("Ignored till http error responses is implemented")
    @Test
    public void addRole_PassedInvalidJson_ReturnsTheCorrectErrorMessage() throws Exception {
        throw new Exception("This test needs to be implemented");
    }

    private String readInJson(String path) throws Exception {
        return JsonReader.readJsonFileInAsAString(getClass().getResource(path));
    }
}