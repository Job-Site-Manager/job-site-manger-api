package api.mapper;

import api.config.Profiles;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.ibatis.jdbc.SqlRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

import java.util.List;
import java.util.Map;

import static org.apache.ibatis.io.Resources.getResourceAsReader;

@RunWith(SpringRunner.class)
@ActiveProfiles(Profiles.DATABASE)
@SpringBootTest
public abstract class AbstractMapperTest {

    @Autowired
    DataSource dataSource;

    SqlRunner sqlRunner;
    ScriptRunner scriptRunner;


    @Before
    public void setup() throws Exception {
        // TODO: should move this into @BeforeClass
        // Need to Determine how to set up Datasource in @BeforeClass method
        sqlRunner = new SqlRunner(dataSource.getConnection());
        scriptRunner = new ScriptRunner(dataSource.getConnection());


        scriptRunner.setAutoCommit(true);
        scriptRunner.setStopOnError(true);
        scriptRunner.runScript(getResourceAsReader("sql-scripts/Create-Database-Schema.sql"));
        scriptRunner.runScript(getResourceAsReader("sql-scripts/data/company-1.sql"));
        scriptRunner.runScript(getResourceAsReader("sql-scripts/data/company-2.sql"));
        scriptRunner.runScript(getResourceAsReader("sql-scripts/data/edge-cases.sql"));
    }

    protected boolean doRowsExistInDatabaseTable(String table, String condition) throws Exception{
        List<Map<String, Object>> results = sqlRunner.selectAll("SELECT * FROM " + table + " WHERE " + condition);

        return results.size() == 0;
    }

    @After
    public void tearDown() {
        sqlRunner.closeConnection();
        scriptRunner.closeConnection();
    }
}
