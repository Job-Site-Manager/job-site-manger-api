package api.mapper;

import api.domain.Employee;
import api.domain.Role;
import api.domain.data.CompanyOne;
import api.domain.data.CompanyTwo;
import api.domain.data.EdgeCases;
import test_utilities.CustomTestAsserts;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

import static org.apache.ibatis.io.Resources.getResourceAsReader;
import static org.assertj.core.api.Assertions.*;


public class EmployeeMapperTest extends AbstractMapperTest {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void selectAllEmployees_FromACompanyWithEmployeesDefined_ReturnsCorrectEmployeeList() {
        CustomTestAsserts.assertListsAreEqualRegardlessOfOrder(
            employeeMapper.selectAllEmployees(CompanyOne.id),
            CompanyOne.EmployeeData.allEmployees
        );
    }

    @Test
    public void selectAllEmployees_NoEmployeesExistForTheCompany_ReturnsAnEmptyList() {
        assertThat(
            employeeMapper.selectAllEmployees(EdgeCases.noDataCompanyId)
        ).isEqualTo(
            Collections.emptyList()
        );
    }

    @Test
    public void selectEmployeeById_PassedAValidEmployeeId_ReturnsTheCorrectEmployee() {
        assertThat(
            employeeMapper.selectEmployeeById(CompanyOne.EmployeeData.tomHouston.getId(), CompanyOne.id)
        ).isEqualTo(
            CompanyOne.EmployeeData.tomHouston
        );
    }

    @Test
    public void selectEmployeeById_IdDoesNotMatchAnyEmployee_ReturnsNull() {
        assertThat(
            employeeMapper.selectEmployeeById(999, CompanyOne.id)
        ).isNull();
    }

    @Test
    public void selectEmployeeById_PassedTheIdOfAnEmployeeFromAnotherCompanyInTheSystem_ReturnsNull() {
        assertThat(
            employeeMapper.selectEmployeeById(CompanyTwo.EmployeeData.billHader.getId(), CompanyOne.id)
        ).isNull();
    }

    @Test
    public void retrieveMatchingNames_WhereSomeOfTheEmployeeNamesExist_ReturnsTheMatchingNames() {
        CustomTestAsserts.assertListsAreEqualRegardlessOfOrder(
            employeeMapper.retrieveMatchingNames(
                Arrays.asList(
                    CompanyOne.EmployeeData.tomHouston.getName(),
                    CompanyOne.EmployeeData.billCrosby.getName(),
                    CompanyTwo.EmployeeData.billHader.getName()
                ),
                CompanyOne.id
            ),
            Arrays.asList(CompanyOne.EmployeeData.tomHouston.getName(), CompanyOne.EmployeeData.billCrosby.getName())
        );
    }

    @Test
    public void retrieveMatchingNames_WhereAllNamesExist_ReturnsTheEntireList() {
        List<String> inputNames = Arrays.asList(
            CompanyOne.EmployeeData.tomHouston.getName(),
            CompanyOne.EmployeeData.billCrosby.getName(),
            CompanyOne.EmployeeData.wayneGretzky.getName()
        );

        CustomTestAsserts.assertListsAreEqualRegardlessOfOrder(
            employeeMapper.retrieveMatchingNames(inputNames, CompanyOne.id),
            inputNames
        );
    }

    @Test
    public void retrieveMatchingNames_NoNamesExist_ReturnsAnEmptyList() {
        assertThat(
            employeeMapper.retrieveMatchingNames(
                Arrays.asList(
                    CompanyTwo.EmployeeData.billHader.getName(),
                    "Unknown Name",
                    "Unknown Name Two"
                ),
                CompanyOne.id
            )
        ).isEqualTo(
            Collections.emptyList()
        );
    }

    @Test(expected = Exception.class)
    public void retrieveMatchingNames_PassedEmptyList_ThrowsAnException(){
        employeeMapper.retrieveMatchingNames(Collections.emptyList(), CompanyOne.id);
    }

    @Test
    public void insertEmployeeWithoutRoles_PassedAValidEmployee_AddsEmployee() {
        int initialNumberOfEmployees = employeeMapper.selectAllEmployees(CompanyOne.id).size();
        String tonyRomoName = "Tony Romo";
        Employee newEmployee = new Employee(
            tonyRomoName,
            "123 New Address",
            "898-343-3242",
            CompanyOne.RoleData.rockTruckOperator,
            new HashSet<>(Collections.singletonList(CompanyOne.RoleData.packerOperator))
        );

        employeeMapper.insertEmployeeWithoutRoles(newEmployee, CompanyOne.id);

        assertThat(
            employeeMapper.selectAllEmployees(CompanyOne.id).size()
        ).isEqualTo(
            initialNumberOfEmployees + 1
        );

        assertThat(
            employeeMapper.selectEmployeeById(newEmployee.getId(), CompanyOne.id)
        ).isEqualTo(
            new Employee(
                newEmployee.getId(),
                newEmployee.getName(),
                newEmployee.getAddress(),
                newEmployee.getPhoneNumber(),
                null,
                new HashSet<>()
            )
        );
    }

    @Test(expected = Exception.class)
    public void insertEmployeeWithoutRoles_EmployeeWithTheSameNameAlreadyExists_ThrowsAnException() {
        employeeMapper.insertEmployeeWithoutRoles(
            CompanyOne.EmployeeData.tomHouston, CompanyOne.id
        );
    }

    @Test
    public void insertPrimaryRole_EmployeeDoesNotHaveAPrimaryRole_AddsRole() {
        String employeeName = "Tony Romo";
        Role primaryRole = CompanyOne.RoleData.packerOperator;

        int employeeId = insertEmployeeWithoutPrimaryOrSecondaryRoles(employeeName, CompanyOne.id);

        employeeMapper.insertPrimaryRole(
            employeeId,
            primaryRole,
            CompanyOne.id
        );

        assertThat(
            employeeMapper.selectEmployeeById(employeeId, CompanyOne.id).getPrimaryRole()
        ).isEqualTo(
            primaryRole
        );
    }

    @Test(expected = Exception.class)
    public void insertPrimaryRole_EmployeeAlreadyHasPrimaryRole_ThrowsException() {
        int companyId = CompanyOne.id;

        employeeMapper.insertPrimaryRole(
            CompanyOne.EmployeeData.tomHouston.getId(),
            CompanyOne.RoleData.packerOperator,
            companyId
        );
    }

    @Test
    public void insertSecondaryRoles_EmployeeHasNoSecondaryRoles_AddsAllRoles(){
        String employeeName = "Tony Romo";
        Set<Role> secondaryRoles = new HashSet<>(Arrays.asList(
            CompanyOne.RoleData.packerOperator, CompanyOne.RoleData.backhoeOperator
        ));

        int employeeId = insertEmployeeWithoutPrimaryOrSecondaryRoles(employeeName, CompanyOne.id);

        employeeMapper.insertSecondaryRoles(employeeId, secondaryRoles, CompanyOne.id);

        assertThat(
            employeeMapper.selectEmployeeById(employeeId, CompanyOne.id).getSecondaryRoles()
        ).isEqualTo(
            secondaryRoles
        );
    }

    @Test
    public void insertSecondaryRoles_PassedNewRoles_AddsRoles(){
        Set<Role> setOfNewRoles = new HashSet<>(Arrays.asList(
            CompanyOne.RoleData.rockTruckOperator, CompanyOne.RoleData.packerOperator
        ));

        employeeMapper.insertSecondaryRoles(
            CompanyOne.EmployeeData.tomHouston.getId(),
            setOfNewRoles,
            CompanyOne.id
        );

        assertThat(
            selectActualSecondaryRoles(CompanyOne.EmployeeData.tomHouston.getId(), CompanyOne.id)
        ).isEqualTo(
            getExpectedSecondaryRoles(CompanyOne.EmployeeData.tomHouston, setOfNewRoles)
        );
    }

    private Set<Role> selectActualSecondaryRoles(int employeeId, int companyId) {
        return employeeMapper.selectEmployeeById(employeeId, companyId).getSecondaryRoles();
    }

    private Set<Role> getExpectedSecondaryRoles(Employee employee, Set<Role> setOfNewRoles) {
        Set<Role> expectedSecondaryRoles = new HashSet<>();
        expectedSecondaryRoles.addAll(employee.getSecondaryRoles());
        expectedSecondaryRoles.addAll(setOfNewRoles);
        return expectedSecondaryRoles;
    }

    @Test(expected = Exception.class)
    public void insertSecondaryRole_EmployeeAlreadyHasSomeOfTheRolesInList_ThrowsException() {
        String employeeName = "Tom Brady";

        int employeeId = insertEmployeeWithSecondaryRoles(
            employeeName,
            new HashSet<>(Arrays.asList(CompanyOne.RoleData.packerOperator, CompanyOne.RoleData.backhoeOperator)),
            CompanyOne.id
        );

        employeeMapper.insertSecondaryRoles(
            employeeId,
            new HashSet<>(Arrays.asList(CompanyOne.RoleData.bulldozerOperator, CompanyOne.RoleData.packerOperator)),
            CompanyOne.id
        );
    }

    @Test
    public void deleteEmployee_EmployeeExistsForCompany_DeletesEmployeeAndPrimarySecondaryRoles() throws Exception {
        int companyId = 1;
        int employeeId = 20;
        scriptRunner.runScript(getResourceAsReader("sql-scripts/data/employee/insert-employee-with-id-20-for-company-1.sql"));

        employeeMapper.deleteEmployee(employeeId, companyId);

        assertThat(employeeMapper.selectEmployeeById(employeeId, companyId)).isNull();
        assertThat(wasPrimaryRoleDeleted(employeeId)).isTrue();
        assertThat(wereSecondaryRolesDeleted(employeeId)).isTrue();
    }

    @Test
    public void deleteEmployee_EmployeeIsFromAnotherCompany_EmployeeNotDeleted() throws Exception {
        int employeeId = 20;
        scriptRunner.runScript(getResourceAsReader("sql-scripts/data/employee/insert-employee-with-id-20-for-company-1.sql"));

        employeeMapper.deleteEmployee(employeeId, 2);

        assertThat(employeeMapper.selectEmployeeById(employeeId, 1)).isNotNull();
        assertThat(wasPrimaryRoleDeleted(employeeId)).isFalse();
        assertThat(wereSecondaryRolesDeleted(employeeId)).isFalse();
    }

    private boolean wasPrimaryRoleDeleted(int employeeId) throws Exception {
        return doRowsExistInDatabaseTable("primary_role ", " employee_id = " + employeeId);
    }

    private boolean wereSecondaryRolesDeleted(int employeeId) throws Exception {
        return doRowsExistInDatabaseTable("secondary_role ", " employee_id = " + employeeId);
    }

    @Test
    public void deletePrimaryRole_EmployeeExistsForCompany_PrimaryRoleIsDeleted() {
        employeeMapper.deletePrimaryRole(CompanyOne.EmployeeData.tomHouston.getId());

        assertThat(employeeMapper.selectEmployeeById(
            CompanyOne.EmployeeData.tomHouston.getId(), CompanyOne.id).getPrimaryRole()
        ).isNull();
    }

    @Test
    public void deleteSecondaryRole_EmployeeExistsForCompany_SecondaryRolesAreDeleted() {
        employeeMapper.deleteSecondaryRoles(CompanyOne.EmployeeData.tomHouston.getId());

        assertThat(
            employeeMapper.selectEmployeeById(CompanyOne.EmployeeData.tomHouston.getId(), CompanyOne.id).getSecondaryRoles()
        ).isEqualTo(Collections.emptySet());
    }

    @Test
    public void updateEmployeeDetails_EmployeeExistsForCompany_EmployeeDetailsUpdated() {
        Employee updatedTom = new Employee(
            CompanyOne.EmployeeData.tomHouston.getId(),
            "Tom Houston",
            "12 Updated Street",
            "613-453-3454",
            CompanyOne.EmployeeData.tomHouston.getPrimaryRole(),
            CompanyOne.EmployeeData.tomHouston.getSecondaryRoles()
        );

        employeeMapper.updateEmployeeDetails(updatedTom, CompanyOne.id);

        assertThat(employeeMapper.selectEmployeeById(CompanyOne.EmployeeData.tomHouston.getId(), CompanyOne.id))
            .isEqualTo(updatedTom);
    }

    @Test
    public void updateEmployeeDetails_EmployeeFromAnotherCompany_EmployeeDetailsNotUpdated() {
        Employee updatedTom = new Employee(
            CompanyOne.EmployeeData.tomHouston.getId(),
            "Tom Houston",
            "12 Updated Street",
            "613-453-3454",
            CompanyOne.EmployeeData.tomHouston.getPrimaryRole(),
            CompanyOne.EmployeeData.tomHouston.getSecondaryRoles()
        );

        employeeMapper.updateEmployeeDetails(updatedTom, CompanyTwo.id);

        assertThat(employeeMapper.selectEmployeeById(CompanyOne.EmployeeData.tomHouston.getId(), CompanyOne.id))
            .isEqualTo(CompanyOne.EmployeeData.tomHouston);
    }

    private int insertEmployeeWithSecondaryRoles(String employeeName, Set<Role> secondaryRoles, int companyId) {
        int employeeId = insertEmployeeWithoutPrimaryOrSecondaryRoles(employeeName, CompanyOne.id);

        employeeMapper.insertSecondaryRoles(
            employeeId,
            secondaryRoles,
            CompanyOne.id
        );

        return employeeId;
    }

    private int insertEmployeeWithoutPrimaryOrSecondaryRoles(String name, int companyId) {
        Employee employee = new Employee(
            name, "123 New Address", "898-343-3242",
            null, new HashSet<>()
        );

        employeeMapper.insertEmployeeWithoutRoles(employee, companyId);

        return employee.getId();
    }

}