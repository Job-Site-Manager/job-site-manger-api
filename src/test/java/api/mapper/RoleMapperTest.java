package api.mapper;

import api.domain.Role;
import api.domain.data.CompanyOne;
import api.domain.data.CompanyTwo;
import api.domain.data.EdgeCases;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

import static org.assertj.core.api.Assertions.*;

public class RoleMapperTest extends AbstractMapperTest {

    @Autowired
    private RoleMapper roleMapper;

    @Test
    public void selectAllRoles_FromACompanyWithRolesDefined_ReturnsCorrectRoleLists() {
        assertThat(
            roleMapper.selectAllRoles(CompanyOne.id)
        ).isEqualTo(
            CompanyOne.RoleData.allRoles
        );
    }

    @Test
    public void selectAllRoles_RolesExistForTheCompany_ReturnsEmptyList() {
        assertThat(
            roleMapper.selectAllRoles(EdgeCases.noDataCompanyId)
        ).isEqualTo(
            Collections.emptyList()
        );
    }

    @Test
    public void selectRoleById_PassedAValidRoleId_ReturnsTheCorrectRole() {
        assertThat(
            roleMapper.selectRoleById(CompanyOne.RoleData.bulldozerOperator.getId(), CompanyOne.id)
        ).isEqualTo(
            CompanyOne.RoleData.bulldozerOperator
        );
    }

    @Test
    public void selectRoleById_IdDoesNotMatchAnyRole_ReturnsNull() {
        assertThat(
            roleMapper.selectRoleById(9999999, CompanyOne.id)
        ).isNull();
    }

    @Test
    public void selectRoleById_PassedIdOfARoleFromAnotherCompany_ReturnsNull() {
        assertThat(
            roleMapper.selectRoleById(CompanyTwo.RoleData.carpenter.getId(), CompanyOne.id)
        ).isNull();
    }

    @Test
    public void selectRoleIdByName_PassedAValidRoleName_ReturnsTheCorrectRoleId() {
        assertThat(
            roleMapper.selectRoleIdByName(CompanyOne.RoleData.bulldozerOperator.getName(), CompanyOne.id)
        ).isEqualTo(
            CompanyOne.RoleData.bulldozerOperator.getId()
        );
    }

    @Test
    public void selectRoleIdByName_NameThatDoesNotMatchAnyRole_ReturnsNull() {
        assertThat(
            roleMapper.selectRoleIdByName("Unknown Name", CompanyOne.id)
        ).isNull();
    }

    @Test
    public void selectRoleIdByName_PassedTheNameOfARoleFromAnotherCompanyInTheSystem_ReturnsNull() {
        assertThat(
            roleMapper.selectRoleIdByName(CompanyTwo.RoleData.carpenter.getName(), CompanyOne.id)
        ).isNull();
    }

    @Test
    public void insertRole_PassedAValidRole_AddsTheRole() {
        int initialNumberOfRoles = roleMapper.selectAllRoles(CompanyOne.id).size();

        Role trackerOperator = new Role("Tractor Operator", "The Tractor operator needs a tractor");

        roleMapper.insertRole(trackerOperator, CompanyOne.id);

        assertThat(
            roleMapper.selectAllRoles(CompanyOne.id).size()
        ).isEqualTo(
            initialNumberOfRoles + 1
        );

        assertThat(
            roleMapper.selectRoleById(trackerOperator.getId(), CompanyOne.id)
        ).isEqualTo(
            trackerOperator
        );
    }

    @Test(expected = Exception.class)
    public void insertRole_RoleWithTheSameNameAlreadyExists_ThrowsException() {
        roleMapper.insertRole(
            new Role(CompanyOne.RoleData.bulldozerOperator.getName(), "Needs to have a backhoe on site", 20),
            CompanyOne.id
        );
    }

    @Test
    public void retrieveMatchingNames_WhereAllNamesExist_ReturnsTheEntireList() {
        Set<String> inputNames = new HashSet<>(Arrays.asList(
            CompanyOne.RoleData.bulldozerOperator.getName(),
            CompanyOne.RoleData.backhoeOperator.getName(),
            CompanyOne.RoleData.rockTruckOperator.getName()
        ));

        assertThat(
            roleMapper.retrieveMatchingNames(inputNames, CompanyOne.id)
        ).isEqualTo(
            inputNames
        );
    }

    @Test
    public void retrieveMatchingNames_WhereSomeOfTheRoleNamesExist_ReturnsTheMatchingNames() {
        assertThat(
            roleMapper.retrieveMatchingNames(
                new HashSet<>(Arrays.asList(
                    CompanyOne.RoleData.bulldozerOperator.getName(),
                    CompanyOne.RoleData.backhoeOperator.getName(),
                    CompanyTwo.RoleData.carpenter.getName()
                )),
                CompanyOne.id
            )
        ).isEqualTo(
            new HashSet<>(Arrays.asList(
                CompanyOne.RoleData.bulldozerOperator.getName(),
                CompanyOne.RoleData.backhoeOperator.getName()
            ))
        );
    }

    @Test
    public void retrieveMatchingNames_NoNamesExist_ReturnsAnEmptyList() {
        assertThat(
            roleMapper.retrieveMatchingNames(
                new HashSet<>(Arrays.asList(
                    CompanyTwo.RoleData.carpenter.getName(),
                    "Unknown Name",
                    "Unknown Name Two"
                )),
                CompanyOne.id
            )
        ).isEqualTo(
            Collections.emptySet()
        );
    }

    // TODO: Need to guard against this at a higher level
    @Test(expected = Exception.class)
    public void retrieveMatchingNames_PassedEmptyList_ThrowsError(){
        roleMapper.retrieveMatchingNames(new HashSet<>(), CompanyOne.id);
    }

    @Test
    public void deleteRole_PassedValidRoleId_DeletesTheRole(){
        int originalNumberOfRoles = roleMapper.selectAllRoles(CompanyOne.id).size();

        roleMapper.deleteRole(CompanyOne.RoleData.labourer.getId() , CompanyOne.id);

        assertThat(roleMapper.selectRoleById(CompanyOne.RoleData.labourer.getId(), CompanyOne.id)).isNull();
        assertThat(roleMapper.selectAllRoles(CompanyOne.id).size()).isEqualTo(originalNumberOfRoles - 1);
    }

    @Test
    public void updateRole_PassedUpdatedRole_UpdatesRole() {
        int bulldozerId = CompanyOne.RoleData.bulldozerOperator.getId();
        Role updatedBulldozer = new Role("Bulldozer", "Anyone can drive it", bulldozerId);

        roleMapper.updateRole(updatedBulldozer, CompanyOne.id);

        assertThat(roleMapper.selectRoleById(bulldozerId, CompanyOne.id)).isEqualTo(updatedBulldozer);
    }

    @Test
    public void updateRole_PassedRoleFromWrongCompany_DoesNotUpdateRole() {
        int bulldozerId = CompanyOne.RoleData.bulldozerOperator.getId();
        Role updatedBulldozer = new Role("Bulldozer", "Anyone can drive it", bulldozerId);

        roleMapper.updateRole(updatedBulldozer, CompanyTwo.id);

        assertThat(roleMapper.selectRoleById(bulldozerId, CompanyOne.id))
            .isEqualTo(CompanyOne.RoleData.bulldozerOperator);
    }

}