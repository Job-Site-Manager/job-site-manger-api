package api.mapper;

import api.domain.*;
import api.domain.data.CompanyOne;
import api.domain.data.CompanyTwo;
import api.domain.data.EdgeCases;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test_utilities.CustomTestAsserts;

import java.util.*;

import static org.apache.ibatis.io.Resources.getResourceAsReader;
import static org.assertj.core.api.Assertions.*;

public class JobMapperTest extends AbstractMapperTest {

    @Autowired
    private JobMapper jobMapper;

    @Test
    public void selectAllJobs_FromACompanyWithJobsDefined_ReturnsTheListOfJobs() {
        CustomTestAsserts.assertListsAreEqualRegardlessOfOrder(
            jobMapper.selectAllJobs(CompanyOne.id),
            CompanyOne.JobData.allJobs
        );
    }

    @Test
    public void selectAllJobs_NoJobsExistForTheCompany_ReturnsAnEmptyList() {
        assertThat(
            jobMapper.selectAllJobs(EdgeCases.noDataCompanyId)
        ).isEqualTo(
            Collections.emptyList()
        );
    }

    @Test
    public void selectJobById_PassedAValidJobId_ReturnsTheCorrectJob() {
        assertThat(
            jobMapper.selectJobById(CompanyOne.JobData.theOldPond.getId(), CompanyOne.id)
        ).isEqualTo(
            CompanyOne.JobData.theOldPond
        );
    }

    @Test
    public void selectJobById_IdDoesNotMatchAnyJob_ReturnsNull() {
        assertThat(
            jobMapper.selectJobById(9999, CompanyOne.id)
        ).isNull();
    }

    @Test
    public void selectJobById_PassedIdOfAJobFromAnotherCompany_ReturnsNull() {
        assertThat(
            jobMapper.selectJobById(CompanyTwo.JobData.theBigHouse.getId() , CompanyOne.id)
        ).isNull();
    }

    @Test
    public void selectJobPositionIdByJobIdAndRoleId_PassedValidIds_ReturnsTheCorrectJobPositionId() {
        assertThat(
            jobMapper.selectJobPositionIdByJobIdAndRoleId(
                CompanyOne.JobData.theOldPond.getId(), CompanyOne.RoleData.bulldozerOperator.getId()
            )
        ).isEqualTo(
            CompanyOne.JobPositionData.bulldozerOperatorAtTheOldPondId
        );
    }

    @Test
    public void selectJobPositionIdByJobIdAndRoleId_PassedIdsThatDoNotMatchAnyJobPositions_ReturnsNull() {
        assertThat(
            jobMapper.selectJobPositionIdByJobIdAndRoleId(102, 1221)
        ).isNull();
    }

    @Test
    public void selectJobPositionById_PassedAValidJobPositionId_ReturnsTheCorrectJobPosition() {
        assertThat(
            jobMapper.selectJobPositionById(CompanyOne.JobPositionData.bulldozerOperatorAtTheOldPondId, CompanyOne.id)
        ).isEqualTo(
            CompanyOne.JobPositionData.bulldozerOperatorAtTheOldPond
        );
    }

    @Test
    public void insertJobWithoutJobPositions_PassedAValidJob_AddsTheJob() {
        Job job = new Job(
            "The new job", "1 Right Street",
            createJobPositions(Collections.singletonList(CompanyOne.RoleData.bulldozerOperator))
        );

        jobMapper.insertJobWithoutJobPositions(job, CompanyOne.id);

        assertThat(
            jobMapper.selectJobById(job.getId(), CompanyOne.id)
        ).isEqualTo(
            new Job(job.getId(), job.getName(), job.getAddress(), new HashSet<>())
        );
    }

    @Test (expected = Exception.class)
    public void insertJobWithoutJobPositions_TheJobNameMatchesTheNameOfAnExistingJob_ThrowsException() {
        jobMapper.insertJobWithoutJobPositions(
            new Job(
                CompanyOne.JobData.theOldPond.getName(), "1 Right Street",
                createJobPositions(Collections.singletonList(CompanyOne.RoleData.bulldozerOperator))
            ),
            CompanyOne.id
        );
    }

    @Test
    public void insertJobPositionsWithoutTheEmployeeLists_PassedValidJobPositions_AddsTheJobPositions() {
        Job job = createBasicJob();
        jobMapper.insertJobWithoutJobPositions(job, CompanyOne.id);

        JobPosition jobPositionBullDozer = createJobPosition(
            CompanyOne.RoleData.bulldozerOperator,
            Collections.singletonList(CompanyOne.EmployeeData.susanMoore)
        );

        JobPosition jobPositionBackhoe = createJobPosition(
            CompanyOne.RoleData.backhoeOperator,
            Arrays.asList(CompanyOne.EmployeeData.timJefferson, CompanyOne.EmployeeData.wayneGretzky)
        );

        jobMapper.insertJobPositionsWithoutTheEmployeeLists(
            createJobPositions(jobPositionBackhoe, jobPositionBullDozer),
            job.getId(),
            CompanyOne.id
        );

        Set<JobPosition> insertedJobPositions = jobMapper.selectJobById(job.getId(), CompanyOne.id).getJobPositions();

        Set<JobPosition> expectedJobPositions = new HashSet<>(Arrays.asList(
            createJobPosition(
                findIdMatchingRole(insertedJobPositions, CompanyOne.RoleData.bulldozerOperator),
                CompanyOne.RoleData.bulldozerOperator,
                Collections.emptyList()
            ),
            createJobPosition(
                findIdMatchingRole(insertedJobPositions, CompanyOne.RoleData.backhoeOperator),
                CompanyOne.RoleData.backhoeOperator,
                Collections.emptyList()
            )
        ));

        assertThat(insertedJobPositions).isEqualTo(expectedJobPositions);
    }

    private int findIdMatchingRole(Set<JobPosition> jobPositionSet, Role role) {
        int id = 0;

        for(JobPosition jobPosition: jobPositionSet) {

            if(role.equals(jobPosition.getRole())) {
                id = jobPosition.getId();
            }
        }

        return id;
    }

    @Test
    public void insertTheEmployeeListForAJobPosition_PassedAValidList_AddsTheList() {
        String jobName = "New Job";
        int jobId = insertJobWithOneJobPosition(jobName, CompanyOne.RoleData.bulldozerOperator, CompanyOne.id);

        int jobPositionId = jobMapper.selectJobPositionIdByJobIdAndRoleId(
            jobId, CompanyOne.RoleData.bulldozerOperator.getId()
        );

        Set<Employee> employeeSet = new HashSet<>(Arrays.asList(
            CompanyOne.EmployeeData.timJefferson,
            CompanyOne.EmployeeData.susanMoore,
            CompanyOne.EmployeeData.wayneGretzky
        ));

        jobMapper.insertTheEmployeeListForAJobPosition(employeeSet, jobPositionId, CompanyOne.id);

        assertThat(
            jobMapper.selectJobPositionById(jobPositionId, CompanyOne.id).getEmployeeSet()
        ).isEqualTo(
            employeeSet
        );
    }

    private int insertJobWithOneJobPosition(String jobName, Role jobPositionRole, int companyId) {
        int jobId = insertJob(jobName, CompanyOne.id);

        jobMapper.insertJobPositionsWithoutTheEmployeeLists(
            createJobPositions(
                createJobPosition(
                    jobPositionRole,
                    Collections.emptyList()
                )
            ),
            jobId,
            companyId
        );

        return jobId;
    }

    @Test
    public void updateJobDetails_PassedUpdatedJob_UpdatesTheJob() {
        int jobId = CompanyOne.JobData.theOldPond.getId();

        Job updatedJob = new Job(
            jobId, "Updated Job Name", "Updated Address", CompanyOne.JobData.theOldPond.getJobPositions()
        );

        jobMapper.updateJobDetails(updatedJob, CompanyOne.id);

        assertThat(jobMapper.selectJobById(jobId, CompanyOne.id)).isEqualTo(updatedJob);
    }

    @Test
    public void updateJobDetails_UpdatingJobFromWrongCompany_DoesNotUpdateJob() {
        int jobId = CompanyOne.JobData.theOldPond.getId();

        Job updatedJob = new Job(
            jobId, "Updated Job Name", "Updated Address", CompanyOne.JobData.theOldPond.getJobPositions()
        );

        jobMapper.updateJobDetails(updatedJob, CompanyTwo.id);

        assertThat(jobMapper.selectJobById(jobId, CompanyOne.id)).isEqualTo(CompanyOne.JobData.theOldPond);
    }

    @Test
    public void retrieveMatchingName_PassedExistingName_ReturnsJobName() {
        String searchedName = CompanyOne.JobData.theOldPond.getName();
        String jobName = jobMapper.retrieveMatchingName(searchedName, CompanyOne.id);

        assertThat(jobName).isEqualTo(searchedName);
    }

    @Test
    public void retrieveMatchingName_PassedNameFromDifferentCompany_ReturnsNull() {
        String searchedName = CompanyTwo.JobData.theBigHouse.getName();
        String jobName = jobMapper.retrieveMatchingName(searchedName, CompanyOne.id);

        assertThat(jobName).isNull();
    }

    @Test
    public void deleteJob_JobExistsForCompany_DeletesJobAndJobPositions() throws Exception {
        int companyId = 1;
        int jobId = 20;
        scriptRunner.runScript(getResourceAsReader("sql-scripts/data/job/insert-job-with-id-20-for-company-1.sql"));

        jobMapper.deleteJob(jobId, companyId);

        assertThat(jobMapper.selectJobById(jobId, companyId)).isNull();
        assertThat(wereJobPositionsDeleted(jobId)).isTrue();
        assertThat(wereEmployeePositionsDeleted(21)).isTrue();
        assertThat(wereEmployeePositionsDeleted(22)).isTrue();
    }

    @Test
    public void deleteJob_JobIsFromAnotherCompany_JobIsNotDeleted() throws Exception {
        scriptRunner.runScript(getResourceAsReader("sql-scripts/data/job/insert-job-with-id-20-for-company-1.sql"));

        jobMapper.deleteJob(20, 2);

        assertThat(jobMapper.selectJobById(20, 1)).isNotNull();
        assertThat(wereJobPositionsDeleted(20)).isFalse();
        assertThat(wereEmployeePositionsDeleted(21)).isFalse();
        assertThat(wereEmployeePositionsDeleted(22)).isFalse();
    }

    @Test
    public void deleteJobPositions_JobExists_DeletesJobPortionsAndEmployee() throws Exception {
        scriptRunner.runScript(getResourceAsReader("sql-scripts/data/job/insert-job-with-id-20-for-company-1.sql"));

        jobMapper.deleteJobPositions(20);

        assertThat(jobMapper.selectJobById(20, 1)).isNotNull();
        assertThat(wereJobPositionsDeleted(20)).isTrue();
        assertThat(wereEmployeePositionsDeleted(21)).isTrue();
        assertThat(wereEmployeePositionsDeleted(22)).isTrue();
    }

    private boolean wereJobPositionsDeleted(int jobId) throws Exception {
        return doRowsExistInDatabaseTable("job_position ", " job_id = " + jobId);
    }

    private boolean wereEmployeePositionsDeleted(int jobPositionId) throws Exception {
        return doRowsExistInDatabaseTable("employee_position ", " job_position_id = " + jobPositionId);
    }

    private Set<JobPosition> createJobPositions(List<Role> roleList) {
        List<JobPosition> jobPositionList = new ArrayList<>();

        roleList.forEach(role -> {
            jobPositionList.add(new JobPosition(role, 2, new HashSet<>()));
        });

        return new HashSet<>(jobPositionList);
    }

    private Set<JobPosition> createJobPositions(JobPosition ...jobPositionList) {
        return new HashSet<>(Arrays.asList(jobPositionList));
    }

    private Job createBasicJob() {
        return new Job("New Job", "1 Right Street", createJobPositions());
    }

    private int insertJob(String jobName, int companyId) {
        Job newJob = new Job(jobName, "1 Right Street", createJobPositions());
        jobMapper.insertJobWithoutJobPositions(newJob, companyId);

        return newJob.getId();
    }

    private JobPosition createJobPosition(int id, Role role, List<Employee> employeeList) {
        return new JobPosition(id, role, 2, new HashSet<>(employeeList));
    }

    private JobPosition createJobPosition(Role role, List<Employee> employeeList) {
        return createJobPosition(0, role, employeeList);
    }
}
