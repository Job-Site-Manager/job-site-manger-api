package api.validation.employee.helpers;

import api.domain.Role;
import api.domain.data.CompanyOne;
import api.repository.AllRoles;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.inOrder;

@RunWith(MockitoJUnitRunner.class)
public class NewEmployeeValidationTest {

    @Mock private AllRoles allRoles;

    private NewEmployeeValidation newEmployeeValidation;

    @Before
    public void setup() {
        Mockito.when(allRoles.contains(any())).thenReturn(true);
        newEmployeeValidation = new NewEmployeeValidation(allRoles);
    }

    @Test(expected = IllegalStateException.class)
    public void validate_PassedEmployeeWhosePrimaryRoleDoesNotExist_ThrowsException() {
        Set<String> primaryRoleNameSet = new HashSet<>(
            Collections.singletonList(CompanyOne.EmployeeData.tomHouston.getPrimaryRole().getName())
        );

        Mockito.when(allRoles.contains(primaryRoleNameSet)).thenReturn(false);

        newEmployeeValidation.validate(CompanyOne.EmployeeData.tomHouston);

        verify(allRoles).contains(primaryRoleNameSet);
    }

    @Test(expected = IllegalStateException.class)
    public void validate_PassedEmployeeWhoseSecondaryRoleDoesNotExist_ThrowsException() {
        Set<String> secondaryRoleNameSet = CompanyOne.EmployeeData.tomHouston.getSecondaryRoles()
            .stream()
            .map(Role::getName)
            .collect(Collectors.toSet());

        Mockito.when(allRoles.contains(secondaryRoleNameSet)).thenReturn(false);

        newEmployeeValidation.validate(CompanyOne.EmployeeData.tomHouston);
    }

    @Test
    public void validate_PassedEmployeeWithPrimaryAndSecondaryRoles_ChecksCorrectRoleNames() {
        Set<String> secondaryRoleNameSet = CompanyOne.EmployeeData.tomHouston.getSecondaryRoles()
            .stream()
            .map(Role::getName)
            .collect(Collectors.toSet());

        Set<String> primaryRoleNameSet = new HashSet<>(
            Collections.singletonList(CompanyOne.EmployeeData.tomHouston.getPrimaryRole().getName())
        );

        newEmployeeValidation.validate(CompanyOne.EmployeeData.tomHouston);

        InOrder inOrder = inOrder(allRoles);
        inOrder.verify(allRoles).contains(primaryRoleNameSet);
        inOrder.verify(allRoles).contains(secondaryRoleNameSet);
    }
}