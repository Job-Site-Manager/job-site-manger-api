package api.validation.role.helpers;

import api.domain.Role;
import api.domain.data.CompanyOne;
import api.repository.AllEmployees;
import api.repository.AllJobs;
import api.repository.AllRoles;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class NewRoleValidationTest {

    @Mock private AllRoles allRoles;
    @Mock private AllJobs allJobs;
    @Mock private AllEmployees allEmployees;

    private NewRoleValidation newRoleValidation;

    @Before
    public void setup() {
        newRoleValidation = new NewRoleValidation(allRoles);
    }

    @Test(expected = IllegalStateException.class)
    public void validateNewRole_ContainsUniqueRole_ThrowsError() {
        Mockito.when(allRoles.contains(any())).thenReturn(true);
        newRoleValidation.validate(CompanyOne.RoleData.bulldozerOperator);
    }

    @Test
    public void validateNewRole_PassedValidRole_DoesNotThrowAnError() {
        Role validRole = new Role("Name 1", "Notes1", 20);
        Mockito.when(allRoles.contains(any())).thenReturn(false);
        newRoleValidation.validate(validRole);
    }
}