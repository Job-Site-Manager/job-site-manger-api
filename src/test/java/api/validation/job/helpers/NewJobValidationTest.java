package api.validation.job.helpers;

import api.domain.Employee;
import api.domain.Job;
import api.domain.JobPosition;
import api.domain.Role;
import api.domain.data.CompanyOne;
import api.repository.AllEmployees;
import api.repository.AllJobs;
import api.repository.AllRoles;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class NewJobValidationTest {

    @Mock
    private AllJobs allJobs;
    @Mock private AllRoles allRoles;
    @Mock private AllEmployees allEmployees;

    private NewJobValidation newJobValidation;

    @Before
    public void setup() {
        Mockito.when(allEmployees.contains(any())).thenReturn(true);
        Mockito.when(allRoles.contains(any())).thenReturn(true);
        newJobValidation = new NewJobValidation(allJobs, allEmployees, allRoles);
    }

    @Test(expected = IllegalStateException.class)
    public void validate_PassedJobNameThatAlreadyExists_ThrowsError() {
        String jobName = "The old pond";
        Mockito.when(allJobs.contains(jobName)).thenReturn(true);
        Job job = new Job(jobName, "1 Street", createValidJobPositionSet());

        newJobValidation.validate(job);
    }

    @Test
    public void validate_PassedNewJobName_DoesNotThrowAnError() {
        String jobName = "The New Job";
        Mockito.when(allJobs.contains(jobName)).thenReturn(false);
        Job job = new Job(jobName, "1 Street", createValidJobPositionSet());

        newJobValidation.validate(job);
    }

    @Test
    public void validate_PassedJobWithMultipleJobPositions_PassesCorrectSetToAllRolesContainsMethod() {
        Set<String> roleNameSet = CompanyOne.JobData.theOldPondAssignedRoles
            .stream()
            .map(Role::getName)
            .collect(Collectors.toSet());

        newJobValidation.validate(CompanyOne.JobData.theOldPond);

        verify(allRoles).contains(roleNameSet);
    }

    @Test(expected = IllegalStateException.class)
    public void validate_ContainsInvalidRole_ThrowsError() {
        Mockito.when(allRoles.contains(any())).thenReturn(false);

        newJobValidation.validate(CompanyOne.JobData.theOldPond);
    }

    @Test
    public void validate_PassedJobWithMultipleJobPositions_PassesCorrectListToAllEmployeesContainsMethod() {
        List<String> employeeNameList = CompanyOne.JobData.theOldPondAssignedEmployees
            .stream()
            .map(Employee::getName)
            .collect(Collectors.toList());

        newJobValidation.validate(CompanyOne.JobData.theOldPond);

        verify(allEmployees).contains(employeeNameList);
    }

    @Test(expected = IllegalStateException.class)
    public void validate_ContainsInvalidEmployee_ThrowsError() {
        Mockito.when(allEmployees.contains(any())).thenReturn(false);

        newJobValidation.validate(CompanyOne.JobData.theOldPond);
    }

    @Test
    public void validate_PassedJobWithValidJobPositions_DoesNotThrowAnError() {
        Job validJob = new Job("Job 1", "1 Street", createValidJobPositionSet());

        newJobValidation.validate(validJob);
    }

    @Test(expected = IllegalStateException.class)
    public void validate_JobHasARoleThatAppearsInMultipleJobPositions_ThrowsCorrectException() {
        Job invalidJob = new Job(
            "Job 1",
            "1 Street",
            JobPositionSetWithRoleAppearingMultipleTimes.getJobPositionSet()
        );

        newJobValidation.validate(invalidJob);
    }

    private Set<JobPosition> createValidJobPositionSet() {
        return new HashSet<>(Arrays.asList(
            CompanyOne.JobPositionData.bulldozerOperatorAtTheGravelPit,
            CompanyOne.JobPositionData.packerOperatorAtTheGravelPit
        ));
    }

    static class JobPositionSetWithRoleAppearingMultipleTimes {

        private static Role role = CompanyOne.RoleData.bulldozerOperator;

        private static Set<JobPosition> jobPositionSet = new HashSet<>(Arrays.asList(
            new JobPosition(
                role,
                2,
                new HashSet<>(Collections.singletonList(CompanyOne.EmployeeData.tomHouston))
            ),
            new JobPosition(
                CompanyOne.RoleData.scraperOperator,
                2,
                new HashSet<>(Collections.singletonList(CompanyOne.EmployeeData.wayneGretzky))
            ),
            new JobPosition(
                role,
                2,
                new HashSet<>(Collections.singletonList(CompanyOne.EmployeeData.tomRosen))
            )
        ));

        static Set<JobPosition> getJobPositionSet() {
            return jobPositionSet;
        }

        static Role getRole() {
            return role;
        }
    }

}
