package test_utilities;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class CustomTestAsserts {

    public static void assertListsAreEqualRegardlessOfOrder(List<?> expectedList, List<?> actualList) {
        assertThat(actualList, containsInAnyOrder(expectedList.toArray()));
    }

    public static void assertActualJsonEqualsExpectedJson(String actualJson, String expectedJson) {
        JSONAssert.assertEquals(expectedJson, actualJson, JSONCompareMode.STRICT);
    }

    public static void assertActualJsonEqualsExpectedJsonInLenientMode(String actualJson, String expectedJson) {
        JSONAssert.assertEquals(expectedJson, actualJson, JSONCompareMode.LENIENT);
    }

}
