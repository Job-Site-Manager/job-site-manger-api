INSERT
	employee(employee_id, name, address, phone_number, company_id)
VALUES
	(20, "Tom Finigin", "12 Steep Road", "519-425-6434", 1);

INSERT
	primary_role (employee_id, role_id)
VALUES
	(20, 1);

INSERT
	secondary_role (employee_id, role_id)
VALUES
	(20,  2),
  (20, 4);