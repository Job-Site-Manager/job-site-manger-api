INSERT
	job(job_id, name, address, company_id)
VALUES
	(20, "The New Job", "23 Pond Rd.", 1);

INSERT
	job_position(job_position_id, role_id, ideal_number_of_employees, job_id)
VALUES
	(21, 1, 2, 20),
  (22,2,1,20);

INSERT
	employee_position(employee_id, job_position_id)
VALUES
	(1, 21),
	(2, 21),
  (3, 22);
