USE JobSiteManager;

-- **************************************
--            INSERT COMPANY
-- **************************************
INSERT
	company (company_id, name, address, phone_number)
VALUES
	(2, "Toms Construction", "13 Beach Road", "613-332-8758");


-- **************************************
--            INSERT ROLES
-- **************************************
INSERT
	role (role_id, name, notes, company_id)
VALUES
	(6, "Carpenter", "Needs a hammer", 2),
  (7, "Plumber", null, 2);


-- **************************************
--            INSERT EMPLOYEES
-- **************************************
INSERT
	employee(employee_id, name, address, phone_number, company_id)
VALUES
	(9, "Bill Hader", "23 Dundas St.", "613-323-5433", 2),
  (10, "Tom Stropingly", "12 Left Lane", "432-342-2423", 2);

INSERT
	primary_role (employee_id, role_id)
VALUES
	(9, 6),
  (10, 7);

INSERT
	secondary_role (employee_id, role_id)
VALUES
	(9, 7),
  (10, 6);


-- **************************************
--            INSERT JOBS
-- **************************************
INSERT
	job(job_id, name, address, company_id)
VALUES
	(3, "The Big House", "425 Talbot  Blvd.", 2);

INSERT
	job_position(job_position_id, role_id, ideal_number_of_employees, job_id)
VALUES
	(9, 6, 2, 3),
  (10, 7, 1, 3);

INSERT
	employee_position(employee_id, job_position_id)
VALUES
	(9, 9),
	(10, 10);
    
    