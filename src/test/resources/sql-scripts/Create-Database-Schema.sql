DROP DATABASE IF EXISTS JobSiteManager;
CREATE DATABASE JobSiteManager;
USE JobSiteManager;

CREATE TABLE company (
	company_id int UNIQUE NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    address varchar(255),
    phone_number varchar(255),
    PRIMARY KEY (company_id)
);

CREATE TABLE role (
	role_id int UNIQUE NOT NULL AUTO_INCREMENT,
	name varchar(255) UNIQUE NOT NULL,
    notes text,
    company_id int NOT NULL,
    FOREIGN KEY (company_id) REFERENCES company(company_id),
    PRIMARY KEY (role_id),
    CONSTRAINT all_role_names_for_a_company_are_unique UNIQUE KEY (name, company_id)
);

CREATE TABLE employee (
	employee_id int UNIQUE NOT NULL AUTO_INCREMENT,
	name varchar (255) NOT NULL,
    address varchar(255),
    phone_number varchar(255),
    company_id int NOT NULL,
    FOREIGN KEY (company_id) REFERENCES company(company_id),
    PRIMARY KEY (employee_id),
    CONSTRAINT all_employee_names_for_a_company_are_unique UNIQUE KEY (name, company_id)
);

-- TODO:
-- There is a limit of only one primary role: need to update the code to reflect this
-- It should not be a duplicate of an employees secondary role
CREATE TABLE primary_role (
    employee_id int NOT NULL UNIQUE,
    role_id int NOT NULL,
    FOREIGN KEY (employee_id) REFERENCES employee(employee_id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES role(role_id),
    CONSTRAINT employee_assigned_role_at_most_once UNIQUE KEY (employee_id, role_id)
);

-- TODO:
-- Should this be handled in the code or at the DB layer or both?
CREATE TABLE secondary_role (
    employee_id int NOT NULL,
    role_id int NOT NULL,
    FOREIGN KEY (employee_id) REFERENCES employee(employee_id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES role(role_id),
    CONSTRAINT employee_assigned_role_at_most_once UNIQUE KEY (employee_id, role_id)
);

CREATE TABLE job (
	job_id int UNIQUE NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
    address varchar(255),
    company_id int,
    FOREIGN KEY (company_id) REFERENCES company(company_id),
    PRIMARY KEY (job_id),
    CONSTRAINT all_job_names_for_a_company_are_unique UNIQUE KEY (name, company_id)
);

CREATE TABLE job_position (
	job_position_id int UNIQUE NOT NULL AUTO_INCREMENT,
    role_id int NOT NULL,
    ideal_number_of_employees int NOT NULL,
    job_id int NOT NULL,
    FOREIGN KEY (job_id) REFERENCES job(job_id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES role(role_id),
    PRIMARY KEY (job_position_id),
    CONSTRAINT each_job_has_role_at_most_once UNIQUE KEY (role_id, job_id)
);

CREATE TABLE employee_position (
	employee_id int NOT NULL,
    job_position_id int NOT NULL,
    FOREIGN KEY (employee_id) REFERENCES employee(employee_id),
    FOREIGN KEY (job_position_id) REFERENCES job_position(job_position_id)  ON DELETE CASCADE,
    CONSTRAINT employee_should_be_assigned_to_job_position_at_most_once PRIMARY KEY (employee_id, job_position_id)
);